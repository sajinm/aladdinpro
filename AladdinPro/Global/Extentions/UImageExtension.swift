//
//  UImageExtension.swift
//  AladdinPro
//
//  Created by Sajin M on 12/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
  func mergeWith(topImage: UIImage) -> UIImage {
    let bottomImage = self

  let size = CGSize(width: topImage.size.width, height: topImage.size.height + bottomImage.size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)

    topImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: topImage.size.height))
    bottomImage.draw(in: CGRect(x: 0, y: topImage.size.height, width: size.width, height: bottomImage.size.height))

    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    //set finalImage to IBOulet UIImageView
    
    return newImage
  }
}


extension UIImage{
    func resize(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}



extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }

 
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
}

extension UIImage {

    func blackAndWhite () -> UIImage?
    {
        
        
        let ciImage = CIImage(image: self)!
        let blackAndWhiteImage = ciImage.applyingFilter("CIColorControls", parameters: ["inputSaturation": 0, "inputContrast": 1])
       
        let context:CIContext = CIContext.init(options: nil)
                  let cgImage:CGImage = context.createCGImage(blackAndWhiteImage, from: blackAndWhiteImage.extent)!
                  let image:UIImage = UIImage.init(cgImage: cgImage)
                  return image
    }

    

}

