//
//  String+Email.swift
//  AladdinPro
//
//  Created by Sajin M on 3/28/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    
    
    func formatWithMonth(date:Date) -> String {
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        if myStringafd.count > 0 {
            return myStringafd
            
        }
        else{
            
            return ""
        }
        
        //        if let date = dateFormatterGet.string(from: date) {
        //            return(dateFormatterPrint.string(from: date))
        //        } else {
        //            return ""
        //        }
        
        
    }
    
    
    
    func formatDate(date:Date) -> String {
        
        
        //print(date)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        if myStringafd.count > 0 {
            return myStringafd
            
        }
        else{
            
            return ""
        }
        
//        if let date = dateFormatterGet.string(from: date) {
//            return(dateFormatterPrint.string(from: date))
//        } else {
//            return ""
//        }
        
        
    }
    
    
    
    
    
   
        
        func fileName() -> String {
            return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
        }
        
        func fileExtension() -> String {
            return NSURL(fileURLWithPath: self).pathExtension ?? ""
        }
 
    
    
}
