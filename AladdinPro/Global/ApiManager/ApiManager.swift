//
//  ApiManager.swift
//  AladdinPro
//
//  Created by Sajin M on 3/27/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import PDFKit

class ApiManager {

    private init () {}
    static let shared = ApiManager()
    var usrToken:String?
    
    
    var userTokenString:String?
    

    
    
    
    
    func userToken() -> String {
        
        var usrToken:String? = ""
        
        if let userToken = GlobalDefault.value(forKey: "userToken"){
            
            usrToken = userToken as? String
        }
        
        return usrToken!
    }
    
    
    
    func doLogin(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,ProfileModel?,String) -> Void))  {
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  URLEncoding.default, headers:nil ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(ProfileModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:ProfileModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
            
            }
            
            
            
        }
        
    }
    
    
    
    func forgotPassword(fromUrl url: URL,withParameter:[String : Any], completion: @escaping ((Bool,String)->Void)){
        
        
        
       
        
        let header = [
          
            "Content-Type":"application/json"
        ]
        
       
        
        
        
  
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.prettyPrinted, headers:header ).responseData {
            (responseData) -> Void in
           
         
            
            do {
                //here dataResponse received from a network request
                
                let info = try JSON(data: responseData.data!)
                
                
                

                DispatchQueue.main.async {
                    
                    if let data = info.dictionary{
                        
                    let message = data["message"]?.stringValue
                        
                        completion(true,message!)
                        
                    }
                    
                   
                }
                
                
                
            } catch let parsingError {
                let model:GeneralModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
        
        
        
    }
    
    
    func getBilling(fromUrl url: URL, completion: @escaping ((Bool,BillingModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
           
            do {
                //here dataResponse received from a network request
                
                
               
                
                
                let decoder = JSONDecoder()
                let model = try decoder.decode(BillingModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message ?? "")
                }
                
                
                
            } catch let parsingError {
                let model:BillingModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    func getPackages(fromUrl url: URL, completion: @escaping ((Bool,PackageModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(PackageModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                
                
                
            } catch let parsingError {
                let model:PackageModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    func AddNewReminder(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,AddCategoryModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(AddCategoryModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                
                
                
            } catch let parsingError {
                let model:AddCategoryModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    @available(iOS 11.0, *)
    func uploadImage(fromUrl url: URL, withParameter:[String : Any], imgArray:PDFDocument, completion: @escaping (Bool,JSON) -> Void)  {
        
        let userToken = self.userToken()
       
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key,value) in withParameter {
                    if let value = value as? String {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
                
                
                //PDF Upload
                
               if imgArray.pageCount > 0 {
                    

                        
                   let fileName = "pdf"
                    let withName = "docFile"
                        
                        multipartFormData.append(imgArray.dataRepresentation()!, withName: withName, fileName: fileName, mimeType: "application/pdf")

            
                                     
                                 }
                

        },
            to: url,
            method: .post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                         print(progress)
                    })
                    
                    upload.responseJSON { response in
                        
                        // If the request to get activities is succesfull, store them
                        if response.result.isSuccess{
                            
                            
                            let jsonData = JSON(response.result.value!)
                            
                            
                            DispatchQueue.main.async {
                                completion(true,jsonData)
                            }
                            
                        } else {
                            
                            print(response.result.value!)
                            
                            
                            let jsonData = JSON(response.result.value!)
                            DispatchQueue.main.async {
                                completion(false,jsonData)
                            }
                            
                            // let errorMessage = "ERROR MESSAGE: "
                            //                            if let data = response.data {
                            //                                // Print message
                            //                                let responseJSON = JSON(data: data)
                            //                                if let message: String = responseJSON["error"]["message"].string {
                            //                                    if !message.isEmpty {
                            //                                        errorMessage += message
                            //                                    }
                            //                                }
                            //                            }
                            //                            print(errorMessage) //Contains General error message or specific.
                            //
                            //   print(response.debugDescription)
                            
                            //SVProgressHUD.dismiss()
                        }
                        
                        //                        alert.dismiss(animated: true, completion:
                        //                            {
                        //                                self.dismiss(animated: true, completion:nil)
                        //
                        //                        })
                    }
                case .failure(let encodingError):
                    
                    print(encodingError)
                    //                    print(encodingError)
                }
        }
        )
        
        
        
        
    }
    
    
    func removeImage(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,AddCategoryModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(AddCategoryModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                
                
                
            } catch let parsingError {
                let model:AddCategoryModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    func AddNewCategory(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,AddCategoryModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(AddCategoryModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                
                
                
            } catch let parsingError {
                let model:AddCategoryModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    func paymentProcess(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,DocumentModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  URLEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            
        }
        
    }
   
    
    
    
    func getDocuments(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,DocumentModel?,String) -> Void))  {
        
        
      
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
     
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(DocumentModel.self, from:
                    responseData.data!) //Decode JSON Response Data
               
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:DocumentModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    
    func getCompanies(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,CompanyModel?,String) -> Void))  {
        
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(CompanyModel.self, from:
                    responseData.data!) //Decode JSON Response Data
           
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:CompanyModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    func getDocForCategory(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,SelectedDocModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(SelectedDocModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:SelectedDocModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    func deleteDoc(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,DeleteDocModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(DeleteDocModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:DeleteDocModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    func updatePassword(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,GeneralModel?,String))-> Void){
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post,parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion((true,model,model.message))
                }
                
                
                
            } catch let parsingError {
                let model:GeneralModel? = nil
                
                
                DispatchQueue.main.async {
                    completion((false,model,parsingError.localizedDescription))
                }
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    
    
    
    func updateProfile(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,GeneralModel?,String))-> Void){
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post,parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion((true,model,model.message))
                }
                
                
                
            } catch let parsingError {
                let model:GeneralModel? = nil
                
                
                DispatchQueue.main.async {
                    completion((false,model,parsingError.localizedDescription))
                }
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    func getCountryList(fromUrl url: URL, completion: @escaping ((Bool,CountryModel?,String))-> Void){
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(CountryModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion((true,model,model.message!))
                }
                
                
                
            } catch let parsingError {
                let model:CountryModel? = nil
                
                
                DispatchQueue.main.async {
                    completion((false,model,parsingError.localizedDescription))
                }
                
            }
            
            
            
        }
        
        
        
    }
    
    
    func editDocument(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,DeleteDocModel?,String) -> Void))  {
        
        
        
        let userToken = self.userToken()
        
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
            
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(DeleteDocModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:DeleteDocModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    func getCategories(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,CategoryModel?,String) -> Void))  {
        
        
        let userToken = self.userToken()
        let headers = [
            
            "Authorization": "Bearer"+userToken,
            "Content-Type": "application/json"
        ]
        
       
        Alamofire.request(url , method: .post, parameters: withParameter, encoding:  JSONEncoding.default, headers:headers ).responseData {
            (responseData) -> Void in
            
           
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(CategoryModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(true,model,model.message!)
                }
                
                
                
            } catch let parsingError {
                let model:CategoryModel? = nil
                
                
                DispatchQueue.main.async {
                    completion(false,model,parsingError.localizedDescription)
                }
                
            }
            
            
            
        }
        
    }
    
    
    
}
