//
//  ResponseHandler.swift
//  AladdinPro
//
//  Created by Sajin M on 3/27/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import PDFKit

class ResponseHandler {
    
    var userProfile:ProfileModel?
    var documentData:DocumentModel?
    var categoryData:CategoryModel?
    var selectedData:SelectedDocModel?
    var packageData:PackageModel?
    var companyData:CompanyModel?
    var countryData:CountryModel?
    var updateProfileData:GeneralModel?
    var billingData:BillingModel?
    
    
 
    
    public func doLogin(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: apiUrl+loginApi) else { return }
        ApiManager.shared.doLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, profileModel, message) in
            
            if isSuccess == true {
                
                self.userProfile = profileModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, "")
            } else {
                completion(true, message)
            }
        })
    }
    
    
    public func checkPassword(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: apiUrl+forgotPassword) else { return }
        ApiManager.shared.forgotPassword(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
            
            if isSuccess == true {
                
            //self.userProfile = profileModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func getCountryList(completion: @escaping ((Bool, String) -> Void)){
        
        guard let url = URL(string: apiUrl+countryList) else { return }
        ApiManager.shared.getCountryList(fromUrl: url, completion: { (isSuccess, countryModel, message) in
            
            if isSuccess == true {
                
                self.countryData = countryModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, "")
            } else {
                completion(true, message)
            }
        })
        
        
        
    }
    
    
    
    public func updatePassword(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)){
        
        guard let url = URL(string: apiUrl+updatePasswordApi) else { return }
        ApiManager.shared.updatePassword(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, profileResponseModel, message) in
            
            if isSuccess == true {
                
                self.updateProfileData = profileResponseModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
        
        
        
    }
    
    
    
    public func updateProfile(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)){
        
        guard let url = URL(string: apiUrl+updateProfileApi) else { return }
        ApiManager.shared.updateProfile(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, profileResponseModel, message) in
            
            if isSuccess == true {
                
                self.updateProfileData = profileResponseModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
        
        
        
    }
    
    
    
    
    public func doSignUp(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: apiUrl+signUpApi) else { return }
        ApiManager.shared.doLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, profileModel, message) in
            
            if isSuccess == true {
                
                self.userProfile = profileModel
                //self.locationList = locationModel?.all_locations ?? []
                completion(true, "")
            } else {
                completion(true, message)
            }
        })
    }
    
    
    
    public func getCompanyList(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+getComapnies) else { return }
        ApiManager.shared.getCompanies(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, companyModel, message) in
            
            
            
            if isSuccess == true {
                
                self.companyData = companyModel
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    
    public func getDocumentData(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
       
        guard let url = URL(string: apiUrl+getDocumentsApi) else { return }
        ApiManager.shared.getDocuments(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, documentModel, message) in
            
            
            
            if isSuccess == true {
              
                self.documentData = documentModel
                completion(true, (self.documentData?.message)!)
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func FetchPackages(completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+packageApi) else { return }
        ApiManager.shared.getPackages(fromUrl: url, completion: { (isSuccess, packageModel, message) in
            
            
            
            if isSuccess == true {
                
                self.packageData = packageModel
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func FetchBilling(completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+billingDetailsApi) else { return }
        ApiManager.shared.getBilling(fromUrl: url, completion: { (isSuccess, BillingModel, message) in
            
            
            
            if isSuccess == true {
                
                self.billingData = BillingModel
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    
    public func addNewCategory(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+addNewCategoryApi) else { return }
        ApiManager.shared.AddNewCategory(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, AddCategoryModel, message) in
            
            
            
            if isSuccess == true {
                
                
                if let message = AddCategoryModel?.message{
                    
                    completion(true, message)
                    
                }
                
                //self.documentData = documentModel
                
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    @available(iOS 11.0, *)
    public func updateImage(withParameter theParameter:[String : Any],imageArray:PDFDocument, completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+updateImageFile) else { return }
        ApiManager.shared.uploadImage(fromUrl: url, withParameter: theParameter,imgArray:imageArray,completion: { (isSuccess,json) in
            
            
            // print(JSON.self)
            
            
            if isSuccess == true {
                
                let info = json.dictionary
                
                
                if info?["resultCode"]?.int == 1{
                    
                    let message = info?["message"]?.string
                    
                    completion(true, message ?? "")
                }
                
                
                
                
                
                
            } else {
                completion(false, "message")
            }
        })
    }
    
    
    
    
    
    
    @available(iOS 11.0, *)
    public func addNewReminderWithImage(withParameter theParameter:[String : Any],imageArray:PDFDocument, completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+newReminderApi) else { return }
        ApiManager.shared.uploadImage(fromUrl: url, withParameter: theParameter,imgArray:imageArray,completion: { (isSuccess,json) in
            
            print(url,theParameter)
           
            
            
            if isSuccess == true {
                
                let info = json.dictionary
               
                
                if info?["resultCode"]?.int == 1{
                    
                    let message = info?["message"]?.string
                    
                    completion(true, message ?? "")
                }
                else{
                     let message = info?["message"]?.string
                    completion(true, message ?? "")
                    
                }



                


            } else {
                completion(false, "message")
            }
        })
    }
    
    
    
    
    
    public func addNewReminder(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+newReminderApi) else { return }
        ApiManager.shared.AddNewCategory(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, AddCategoryModel, message) in
            
            
            
            if isSuccess == true {
                
                
                if let message = AddCategoryModel?.message{
                    
                    completion(true, message)
                    
                }
                
                //self.documentData = documentModel
                
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    
    
    
//    public func paymentProcess(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
//        
//        
//        guard let url = URL(string: paymentProcess) else { return }
//        ApiManager.shared.paymentProcess(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, DocDeleteModel, message) in
//            
//            
//            
//            if isSuccess == true {
//                
//                
//                if let message = DocDeleteModel?.message{
//                    
//                    completion(true, message)
//                    
//                }
//                
//                //self.documentData = documentModel
//                
//            } else {
//                completion(false, message)
//            }
//        })
//    }
    
    
    
    public func editDoc(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+editReminder) else { return }
        ApiManager.shared.AddNewCategory(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, DocDeleteModel, message) in
            
            
            
            if isSuccess == true {
                
                
                if let message = DocDeleteModel?.message{
                    
                    completion(true, message)
                    
                }
                
                //self.documentData = documentModel
                
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    
    public func deleteDoc(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+deleteDocuments) else { return }
        ApiManager.shared.AddNewCategory(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, DocDeleteModel, message) in
            
            
            
            if isSuccess == true {
                
                
                if let message = DocDeleteModel?.message{
                    
                    completion(true, message)
                    
                }
                
                //self.documentData = documentModel
                
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func removeImage(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+removeImageApi) else { return }
        ApiManager.shared.removeImage(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, DocDeleteModel, message) in
            
            
            
            if isSuccess == true {
                
                
                if let message = DocDeleteModel?.message{
                    
                    completion(true, message)
                    
                }
                
                //self.documentData = documentModel
                
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func getSelectedCatDocs(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+selectedDocuments) else { return }
        ApiManager.shared.getDocForCategory(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, documentModel, message) in
            
            
            
            if isSuccess == true {
                
                self.selectedData = documentModel
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getCategoryData(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        
        guard let url = URL(string: apiUrl+getCategoryApi) else { return }
        ApiManager.shared.getCategories(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, categoryModel, message) in
            
        
            
            if isSuccess == true {
                
                self.categoryData = categoryModel
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
}
