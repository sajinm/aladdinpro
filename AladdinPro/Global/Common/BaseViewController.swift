//
//  BaseViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 3/26/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift


class BaseViewController: UIViewController {
    
    
    var style = ToastStyle()

    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
    

           // toggle "tap to dismiss" functionality
         

           // toggle queueing behavior
        

        // Do any additional setup after loading the view.
    }
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

    
   
        
    func setToast(message:String,view:UIView,onController:UINavigationController)  {
        
        
                  var style = ToastStyle()
                  style.messageFont = UIFont(name: "Helvetica Neue", size: 13.0)!
                  style.messageColor = UIColor.black
                  style.messageAlignment = .center
                  style.backgroundColor =  #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
                  style.horizontalPadding = 10
        
        
        onController.view.makeToast(message, duration: 3.0, position: .bottom, style:style)


        
//            ToastManager.shared.isQueueEnabled = true
//            let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: 35.0))
//                                                      customView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
//                                           customView.backgroundColor = .darkGray
//
//                                           customView.alpha = 4
//
//                                           let label = UILabel(frame: CGRect(x: 0, y: 0, width:customView.frame.width, height: customView.frame.height))
//                                           label.font = UIFont(name: "Helvetica Neue", size: 14.0)!
//                                           label.textAlignment = .center
//                                           label.text = message
//                                           label.textColor = UIColor.white
//                                           customView.addSubview(label)
//
//
//            onController.view.showToast(customView, duration: 3.0, position: .bottom)

        
        
        
    }


}
 
