//
//  Utils.swift
//  AladdinPro
//
//  Created by Sajin M on 3/30/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit

public enum Severity{
    case Expiring
    case Critical
  
}


class Utils {
    
    static func setType(type:String) -> UIColor{
        
        switch type {
        case "Expiring":
            return UIColor(red: 98 / 255, green: 141 / 255, blue: 186 / 255, alpha: 1)
        case "Critical":
            return UIColor(red: 232/255, green: 59/255, blue: 74/255, alpha: 1)
        case "Expired":
             return UIColor(red: 145/255, green: 145/255, blue: 145/255, alpha: 1)
            
        default:
            return UIColor(red: 98 / 255, green: 141 / 255, blue: 186 / 255, alpha: 1)
        }
    }
    
}

