//
//  Constants.swift
//  AladdinPro
//
//  Created by Sajin M on 3/27/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

let AppTitle:String = "Aladdinpro"
let noInternrt:String = "No Internet Connection"
let validUsername:String = "Please enter a valid username and password"
let mandatoryFields:String = "Please fill all fields"
let validEmail:String = "Please enter a valid email id"
let valideCategoryName:String = "Please enter a valid category name"
let selectCategory:String = "Please select a category"
let nodataFound:String = "No data found!"
let passwordConfirmation = "New Password and Confirm Password should be same"


let GlobalDefault = UserDefaults.standard


let apiUrl:String = "https://app.aladdinpro.com/"
let imageUrl:String = "https://app.aladdinpro.com/documents/"
let loginApi:String = "app-user/login"
let signUpApi:String = "app-user/register"
let getDocumentsApi:String = "api/documents/upcoming-severe-documents"
let getCategoryApi:String = "api/document-types/list-names"
let addNewCategoryApi:String = "api/document-types/create"
let newReminderApi:String = "api/documents/save"
let selectedDocuments:String = "api/documents/get_list"
let deleteDocuments:String = "api/documents/delete-document"
let editReminder:String = "api/documents/update-document"
let paymentProcess:String = "https://www.aladdinpro.com/app_card_payment.php"
let packageApi:String = "api/account-setting/get-user-package"
let removeImageApi:String = "api/documents/remove-file"
let getComapnies:String = "api/app-user/companies"
let updateImageFile:String = "api/documents/update-file"
let forgotPassword:String = "app-user/forgot-password"
let countryList:String = "api/account-setting/get-countries"
let updateProfileApi:String = "api/account-setting/update"
let updatePasswordApi:String = "api/account-setting/update-password"
let billingDetailsApi:String = "api/account-setting/billing"


let supportUrl:String = "https://www.aladdinpro.com/support"
let contact:String = "https://www.aladdinpro.com/contact.php"
let terms:String = "https://www.aladdinpro.com/tos.html"
let privacy:String = "https://www.aladdinpro.com/privacy.html"
let pricingUrl:String = "https://www.aladdinpro.com/pricing.php"

let about:String = "About"
let help:String = "Help"
let pricing:String = "Pricing"
