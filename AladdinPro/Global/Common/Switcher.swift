//
//  Switcher.swift
//  AladdinPro
//
//  Created by Sajin M on 3/29/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit

class Switcher {
    
    static func updateRootVC(){
        
        let status = GlobalDefault.value(forKey: "userToken")
        var rootVC : UIViewController?
        
     
        
        if(status != nil){
            //
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
        }else{
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialNavigation") as! UINavigationController
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
        
    }

    
}
