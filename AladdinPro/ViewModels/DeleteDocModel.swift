//
//  DeleteDocModel.swift
//  AladdinPro
//
//  Created by Sajin M on 4/7/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct DeleteDocModel:Codable {
    var resultCode:Int
    var message:String?
    var resultData:String?
}
