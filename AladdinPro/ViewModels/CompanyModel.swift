//
//  CompanyModel.swift
//  AladdinPro
//
//  Created by Sajin M on 5/13/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct CompanyModel:Codable {
    
    let resultCode:Int
    let message:String?
    let resultData:cmpModel?
    
}

struct cmpModel:Codable {
    
    let total:Int
    let per_page:Int
    let current_page:Int
    let last_page:Int
    let from:Int
    let to:Int
    let data:[CmpDataModel]
    
}

struct CmpDataModel:Codable {
    
    
    let id:String
    let name:String
    let is_default:String
    
    
}

