//
//  CountryModel.swift
//  AladdinPro
//
//  Created by Sajin M on 7/29/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation


struct CountryModel:Codable {
    
    let resultCode:Int
    let message:String?
    let resultData:[Countries]?
    
}


struct Countries:Codable {
    var id:String
    var name:String
}
