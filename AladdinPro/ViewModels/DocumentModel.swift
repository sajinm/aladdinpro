//
//  DocumentModel.swift
//  AladdinPro
//
//  Created by Sajin M on 3/29/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct DocumentModel:Codable {
    
    let resultCode:Int
    let message:String?
    let resultData:dataModel?
    
}

struct dataModel:Codable {
    
    let total:Int
    let per_page:Int
    let current_page:Int
    let last_page:Int
    let from:Int
    let to:Int
    let data:[documentModel]
    
}

struct documentModel:Codable {
    
    
    let id:String
    let document_no:String
    let user_id:String
    let document_type:String
    let document_type_id:String
    let description:String?
    let expiry_date:String
    let notes:String?
    let severity:String
    let is_notifiable:Bool
    let file_name:String?
    let company_name:String?
    let company_id:String?
    let days_left:Int
}
