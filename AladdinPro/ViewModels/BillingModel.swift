//
//  BillingModel.swift
//  AladdinPro
//
//  Created by Sajin M on 8/16/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct BillingModel:Codable {
    var resultCode:Int
    var message:String?
    var resultData:subModel?
}

struct subModel:Codable {
    var subscription: subscriptionModel?
    var package_detail: [packageModel]?
    var transactions: [transactionModel]?
}

struct subscriptionModel: Codable {
    var id: String?
    var key_id : String?
    var key : String
    var membership_id : String
    var name : String
    var subscription_date : String
    var expiry_date: String
    var method : String
    var created_at: String
    var day_left: Int
    var renew_date : String?
    
   
}

struct packageModel:Codable {
    
    var feature:String?
    var used_or_available:String?
    
}

struct transactionModel:Codable {
    var id: String?
    var debit: String?
    var credit: String?
    var transaction_method: String
    var created_at: String?
    var txn_code: String?
    var created_at_local: String?
}
