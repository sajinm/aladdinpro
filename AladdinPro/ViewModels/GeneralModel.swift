//
//  GeneralModel.swift
//  AladdinPro
//
//  Created by Sajin M on 8/2/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct GeneralModel: Codable {
    var resultCode:Int
    var message:String
    var resultData:String?
    
}
