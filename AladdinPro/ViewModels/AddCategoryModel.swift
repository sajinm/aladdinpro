//
//  AddCategoryModel.swift
//  AladdinPro
//
//  Created by Sajin M on 4/3/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit

struct AddCategoryModel:Codable {
    
    let resultCode:Int
    let message:String
    let resultData:String?
    
}
