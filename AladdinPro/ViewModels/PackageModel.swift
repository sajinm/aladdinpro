//
//  PackageModel.swift
//  AladdinPro
//
//  Created by Sajin M on 4/26/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct PackageModel:Codable {

    let resultCode:Int
    let message:String
    let resultData:[resultModelData]
    
}

struct resultModelData:Codable {
    let id:String
    let name:String
    let description:String?
    let price:String
    let is_popular:String
    let frequency:String
    let order:String
    let isCurrent:Bool
    let action:Int
    let details:[Details]
}

struct Details:Codable {
    let attribute_id:String
    let name:String
    let value_data:String
}
