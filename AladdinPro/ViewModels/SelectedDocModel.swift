//
//  SelectedDocModel.swift
//  AladdinPro
//
//  Created by Sajin M on 4/6/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct SelectedDocModel:Codable {
    
    let resultCode:Int
    let message:String?
    let resultData:dataModel?
    
}

struct documentExpModel {
    
    
    let total:Int
    let per_page:Int
    let current_page:Int
    let last_page:Int
    let from:Int
    let to:Int
    let critical_count:Int?
    let expiring_count:Int?
    let expired_count:Int?
    let custom_header_names:headerModel?
    let data:[documentModel]
    
}

struct headerModel:Codable{
    
    let reference_number:String?
    let description:String?
    let due_on:String?
    let is_active:Int
    
}


