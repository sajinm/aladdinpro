//
//  ProfileModel.swift
//  AladdinPro
//
//  Created by Sajin M on 3/27/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation

struct ProfileModel:Codable {

    let resultCode:Int
    let message:String?
    let resultData:resultModel?
    
    
}


struct resultModel:Codable {
    
    let id:String?
    let parent_id:String?
    let cron_job_id:String?
    let group_id:String?
    let reseller_id:String?
    let membership_id:String?
    let membership_serial:String?
    let company_id:String?
    let currency_id:String?
    let language_id:String?
    let timezone_id:String?
    let country_id:String?
    let permission_id:String?
    let name:String?
    let username:String?
    let email:String?
    let password:String?
    let plain_password:String?
    let confirmation_code:String?
    let remember_token:String?
    let confirmed:String?
    let contact:String?
    let address:String?
    let city:String?
    let image:String?
    let balance_amount:String?
    let balance_category:String?
    let user_category:String?
    let notification_time:String?
    let group_by_documents:String?
    let custom_mail_header:String?
    let custom_mail_footer:String?
    let is_active:String?
    let purpose:String?
    let created_at:String?
    let updated_at:String?
    let deleted_at:String?
    let fcm_registration_status:Int?
    let default_company:String?
    let country_name:String?
    let auth_token:String?
}
