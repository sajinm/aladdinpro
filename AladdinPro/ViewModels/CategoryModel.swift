//
//  CategoryModel.swift
//  AladdinPro
//
//  Created by Sajin M on 4/1/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit

struct CategoryModel:Codable {
    
    let resultCode:Int
    let message:String?
    let resultData:catModel?
    
}

struct catModel:Codable {
    
    let total:Int
    let per_page:Int
    let current_page:Int
    let last_page:Int
    let from:Int
    let to:Int
    let data:[categoryData]
    
}

struct categoryData:Codable {
    
    
    let id:String
    let name:String
}

