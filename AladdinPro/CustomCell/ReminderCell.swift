//
//  ReminderCell.swift
//  AladdinPro
//
//  Created by Sajin M on 3/21/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit



//protocol moreOption{
//
//    func moreOptionPressed(at index:IndexPath)
//    
//    
//    
//    
//    
//}

class ReminderCell: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblDayCount: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var circleView: circleView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOpacity = 0.2
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 2
        bgView.layer.cornerRadius = 3
       // bgView.clipsToBounds = true
       // bgView.layer.shadowPath = UIBezierPath(rect: bgView.bounds).cgPath
        bgView.layer.shouldRasterize = false
        
        circleView.makeCircle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
