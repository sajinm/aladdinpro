//
//  PDFSizeTableViewCell.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

class PDFSizeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var menuTitle: UILabel!
    
    @IBOutlet weak var sizeSlider: CustomSlider!
    
    
    
 
    
    var low = 0.5
    var medium = 0.75
    var high = 1.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      
        
        let total: Float = 4
        sizeSlider.maximumValue = total
        sizeSlider.minimumValue = 0
        
        if GlobalDefault.value(forKey: "size") == nil{
            
              GlobalDefault.set(medium, forKey: "size")
            
        }else{
            
            let size = GlobalDefault.value(forKey: "size") as? Float
            
            if size == 0.75 {
                
                
                sizeSlider.value = 2.0
                
                
            }else if let value = size , value > 0.75 {
                
                sizeSlider.value = 4.0
                
            }else{
                
                
                sizeSlider.value = 0
                
            }
            
            
        }
        
      
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func sliderValueChange(_ sender: CustomSlider) {
      
        let step: Float = 2
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        
        print(sender.value)
        
        if sender.value < 2{
            
            GlobalDefault.set(low, forKey: "size")
        }
        
        if sender.value > 2{
            
             GlobalDefault.set(high, forKey: "size")
        }

        if sender.value == 2{
            
             GlobalDefault.set(medium, forKey: "size")
        }
      
        
        
    }
    
    

}
