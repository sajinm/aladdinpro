//
//  ThumbViewCell.swift
//  AladdinPro
//
//  Created by Sajin M on 09/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

protocol ThumbDeleteProtocol {
    
    func removeThumbnail(at index:Int)  

}

class ThumbViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbPic: UIImageView!
    @IBOutlet weak var btnRemove: UIButton!
    
    var indexValue:Int?
    
    var delegate:ThumbDeleteProtocol?
    
    //delegate.removeThumbnail
    
    
    @IBAction func removePressed(_ sender: Any) {
        
        if let index = indexValue {
            
           delegate?.removeThumbnail(at:index)
            
        }
        
        
        
    }
    
    
}
