//
//  PDFColorCell.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

class PDFColorCell: UITableViewCell {
    
    
    
    @IBOutlet weak var menuTitle: UILabel!
    
    @IBOutlet weak var colorSwich: MySegmentControl!
    
    var isMonochrome:Bool?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if GlobalDefault.value(forKey: "isMonochrome") != nil{
            
            self.colorSwich.isOn =  (GlobalDefault.bool(forKey: "isMonochrome"))
        }else{
            
             self.colorSwich.isOn = false
            
        }
        
    
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func colorChange(_ sender: UISwitch) {
        
        if (sender.isOn){
            
            GlobalDefault.set(sender.isOn, forKey: "isMonochrome")
          
        }else{
            
             GlobalDefault.set(sender.isOn, forKey: "isMonochrome")
           
        }
        
        
    }
    
    
    

}
