//
//  MenuCell.swift
//  AladdinPro
//
//  Created by Sajin M on 4/7/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit


protocol menuCellDelgate {
    
    
    func setProfilePic()
    
    
}

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var lblUsername: UILabel!
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var menuTitle: UILabel!
    
    @IBOutlet weak var btnUpgrade: UIButton!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    
    
    
    var delegate:menuCellDelgate?
    
    var userImg:UIImage?
    
    
    @IBOutlet weak var lblPackage: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
    
    
    @IBAction func changeProfilePicPressed(_ sender: UIButton) {
        
        self.delegate?.setProfilePic()
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
