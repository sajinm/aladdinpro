//
//  apButton.swift
//  AladdinPro
//
//  Created by Sajin M on 2/7/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit

class apButton: UIButton {

    let corner_radius : CGFloat =  5.0

    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        self.layer.cornerRadius = corner_radius
        self.clipsToBounds = true
        
        
        
    }
    
    
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
