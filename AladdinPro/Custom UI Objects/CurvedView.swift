//
//  CurvedView.swift
//  AladdinPro
//
//  Created by Sajin M on 08/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//


import Foundation
import UIKit

 class CurvedView: UIButton {
    
//    @IBInspectable var cornerRadius: CGFloat = 10 {
//        didSet {
//            refreshCorners(value: cornerRadius)
//        }
//    }
//
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView(){
        
         layer.cornerRadius = 0
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.0
        // refreshCorners(value: cornerRadius)
      
    }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
}

