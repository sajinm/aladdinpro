//
//  MySegmentControl.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

 @IBDesignable class MySegmentControl: UISwitch {
    
   

    @IBInspectable var height: CGFloat = 0.75 {
            didSet {
                self.transform = CGAffineTransform(scaleX: height, y: height)
            }
        }
   

}
