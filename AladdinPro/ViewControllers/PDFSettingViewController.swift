//
//  PDFSettingViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

class PDFSettingViewController: UIViewController {
    
    
    @IBOutlet weak var listTableView: UITableView!
    
    
    var menuArray = ["Monochrome","Size"]

    override func viewDidLoad() {
        super.viewDidLoad()

        
        initialLoad()
        // Do any additional setup after loading the view.
    }
    
    
    func initialLoad(){
        
                self.listTableView.tableFooterView = UIView()
                self.listTableView.delegate = self
                self.listTableView.dataSource = self
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
           
       }
       
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PDFSettingViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
         if indexPath.row == 0{
        
        let cell = self.listTableView.dequeueReusableCell(withIdentifier: "pdfColorCell", for: indexPath) as! PDFColorCell
        cell.menuTitle.text = menuArray[indexPath.row]
        
        return cell
        }
        
        
        if indexPath.row == 1{
            
            let cell = self.listTableView.dequeueReusableCell(withIdentifier: "pdfSizeCell", for: indexPath) as! PDFSizeTableViewCell
                   cell.menuTitle.text = menuArray[indexPath.row]
                   
                   return cell
            
            
        }
        
        let cell = UITableViewCell()
        return cell
        
    }
    
    
   
    
    
}
