//
//  WebViewViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    @IBOutlet weak var headerTitle: UILabel!
    
    @IBOutlet weak var showView: UIView!
    
     var webView: WKWebView!
    var headerText:String?
    var loadUrl:String?
    

      override func viewDidLoad() {
          super.viewDidLoad()
        
        
        if let title = headerText {
            
            self.headerTitle.text = title
            
        }
        
        
        let webConfiguration = WKWebViewConfiguration()

        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: 0.0, height: self.showView.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.showView.addSubview(webView)
        webView.topAnchor.constraint(equalTo: showView.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: showView.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: showView.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: showView.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: showView.heightAnchor).isActive = true

        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.scrollView.bounces = false
       
        if let webUrl = self.loadUrl{
                   
                    self.openUrl(requestURLString:webUrl)
                   
               }
        

          
      }
    
    
    func openUrl(requestURLString:String) {
    let url = URL (string: requestURLString)
    let request = URLRequest(url: url!)
    webView.load(request)
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
   

}

extension WebViewViewController:WKUIDelegate, WKNavigationDelegate{
    
    
    
    
    
}
