//
//  SignUpViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 2/20/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift

class SignUpViewController: BaseViewController {
    
    
    @IBOutlet weak var btnAlreadyRegistered: apButton!
    
    @IBOutlet weak var txtName: ImageTextField!
    
    @IBOutlet weak var txtUserName: ImageTextField!
    
    @IBOutlet weak var txtEmailId: ImageTextField!
    
    @IBOutlet weak var txtCompanyName: ImageTextField!
    
    @IBOutlet weak var txtPassword: ImageTextField!
    
    var selectedPackage:String?
  //  @IBOutlet weak var txtCountry: ImageTextField!
    
   // var countryPicker = UIPickerView()
    
     let SignUpModel = ResponseHandler()
    
    
    
      //var countryList = [["id": "1","name": "Afghanistan"],["id": "2","name": "Albania"],["id": "3", "name": "Algeria"],["id": "4", "name": "American Samoa"],["id": "5","name": "Andorra"],["id": "6","name": "Angola"],["id": "7","name": "Anguilla"],["id": "8","name": "Antarctica"],["id": "9","name": "Antigua and Barbuda"],["id": "10","name": "Argentina"],["id": "11","name": "Armenia"],["id": "12","name": "Aruba"],["id": "13","name": "Australia"],["id": "14","name": "Austria"],["id": "15","name": "Azerbaijan"],["id": "16","name": "Bahamas"],["id": "17","name": "Bahrain"],["id": "18","name": "Bangladesh"],["id": "19","name": "Barbados"],["id": "20","name": "Belarus"],["id": "21","name": "Belgium"],["id": "22","name": "Belize"],["id": "23","name": "Benin"],["id": "24","name": "Bermuda"],["id": "25","name": "Bhutan"],["id": "26","name": "Bolivia"],["id": "27","name": "Bosnia and Herzegovina"],["id": "28","name": "Botswana"],["id": "29","name": "Bouvet Island"],["id": "30","name": "Brazil"],["id": "31","name": "British Indian Ocean Territory"],["id": "32","name": "Brunei Darussalam"],["id": "33","name": "Bulgaria"],["id": "34","name": "Burkina Faso"],["id": "35","name": "Burundi"],["id": "36","name": "Cambodia"],["id": "37","name": "Cameroon"],["id": "38","name": "Canada"],["id": "39","name": "Cape Verde"],["id": "40","name": "Cayman Islands"],["id": "41","name": "Central African Republic"],["id": "42","name": "Chad"],["id": "43","name": "Chile"],["id": "44","name": "China"],["id": "45","name": "Christmas Island"],["id": "46","name": "Cocos (Keeling) Islands"],["id": "47","name": "Colombia"],["id": "48","name": "Comoros"],["id": "49","name": "Congo"],["id": "50","name": "Congo, the Democratic Republic"],["id": "51","name": "Cook Islands"],]
 
    
    
    



  
   
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpinitialView()
        // Do any additional setup after loading the view.
    }
    
    
    func  setUpinitialView()  {
        
//        countryPicker.delegate = self
//        self.txtCountry.inputView = countryPicker
        
        
        let attrStr = NSMutableAttributedString(string: "Already Registered? Sign In")
        attrStr.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1611390114, green: 0.7553957105, blue: 0.5481526852, alpha: 1), range: NSRange(location: 19, length: 8))
        btnAlreadyRegistered.setAttributedTitle(attrStr, for: .normal)
    }

    
    @IBAction func signInPressed(_ sender: Any) {
        
       performSegue(withIdentifier: "signUpToSignIn", sender: Any?.self)
    }
    
    @IBAction func signUpPressed(_ sender: Any) {
        
       // paymentVC
        
       // performSegue(withIdentifier: "toPaymentView", sender: Any?.self)
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            if (txtName.text?.isEmpty)! ||  (txtPassword.text?.isEmpty)! ||  (txtEmailId.text?.isEmpty)! ||  (txtUserName.text?.isEmpty)! ||  (txtCompanyName.text?.isEmpty)! {
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }else if(!(txtEmailId.text?.isValidEmail())!){
                
                Alertift.alert(title: AppTitle, message:validEmail)
                    .action(.default("OK"))
                    .show(on: self)
                return
            }
            else{
                
                let params = [
                    "name": txtName.text!,
                    "username": txtUserName.text!,
                    "password": txtPassword.text!,
                    "email": txtEmailId.text!,
                    "company_name": txtCompanyName.text!,

                    
                    ] as [String : Any]
                
                SVProgressHUD.show()
                
                SignUpModel.doSignUp(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                       // print(self.SignUpModel.userProfile!)
                        
                        if self.SignUpModel.userProfile?.resultCode == 1{
                            
                            GlobalDefault.set(self.SignUpModel.userProfile?.resultData?.auth_token, forKey: "userToken")
                            
                            // print(GlobalDefault.value(forKey: "userToken")!)
                            
                            if let emailStr = self.SignUpModel.userProfile?.resultData?.email{
                                
                                GlobalDefault.set(emailStr, forKey: "userEmail")
                                
                            }
                            
                            if let contactStr = self.SignUpModel.userProfile?.resultData?.contact{
                                
                                GlobalDefault.set(contactStr, forKey: "userContactNumber")
                                
                            }
                            
                            if let userNameStr = self.SignUpModel.userProfile?.resultData?.username{
                                
                                GlobalDefault.set(userNameStr, forKey: "userName")
                                
                            }
                            
                            if self.selectedPackage != "0"{
                                 self.performSegue(withIdentifier: "toPaymentView", sender: Any?.self)
                                
                            }else{
                                
                                 self.performSegue(withIdentifier: "signUpToHome", sender: Any?.self)
                                
                            }
                            
                           
                           // self.performSegue(withIdentifier: "signUpToPackage", sender: Any?.self)
                            //signUpToHome
                            
                        }
                        else{
                            
                            if let msgStr = self.SignUpModel.userProfile?.message{
                                
                                Alertift.alert(title: AppTitle, message: msgStr)
                                    .action(.default("OK"))
                                    .show(on: self)
                                
                            }
                            
                            
                            
                        }
                        
                        //print(self.LoginModel.userProfile?.message as Any)
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
     // */
    
        
    }
    


}
