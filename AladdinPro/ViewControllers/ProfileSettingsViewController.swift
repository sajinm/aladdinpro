//
//  ProfileSettingsViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/17/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alertift
import SVProgressHUD

class ProfileSettingsViewController: BaseViewController {
    
    @IBOutlet weak var btnCountry: UIButton!
    
    @IBOutlet weak var btnEdit: apButton!
    
    @IBOutlet weak var btnPasswordEdit: apButton!
    
    @IBOutlet weak var txtName: HoshiTextField!
    
    @IBOutlet weak var txtUserName: HoshiTextField!
    
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtContact: HoshiTextField!
    
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var txtCurrentPassword: HoshiTextField!
    @IBOutlet weak var txtNewPassword: HoshiTextField!
    
    @IBOutlet weak var txtConfirmpassword: HoshiTextField!
    
    @IBOutlet weak var countryTableView: UITableView!
    
    @IBOutlet weak var btnSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var txtCountry: HoshiTextField!
    var getPackage = ResponseHandler()
    
    
    var countryId:String = ""
    var contryName:String = ""
    
    
    var apiHandler = ResponseHandler()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let font = UIFont.systemFont(ofSize: 12)
        btnSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                 for: .normal)
        
        self.countryTableView.isHidden = true
        
        self.txtName.isEnabled = false
        self.txtContact.isEnabled = false
        self.txtCurrentPassword.isEnabled = false
        self.txtConfirmpassword.isEnabled = false
        self.txtNewPassword.isEnabled = false
        
        
        self.passwordView.isHidden = true
        if let name = GlobalDefault.value(forKey: "name") {
            
            txtName.text = name as? String
            
        }
        
        if let country = GlobalDefault.value(forKey: "countryName") {
            
            let contryStr = country as? String
            
            self.txtCountry.text = contryStr
            
            //btnCountry.setTitle(contryStr, for: .normal)
        }
        
        
        if let userName = GlobalDefault.value(forKey: "userName") {
            
            txtUserName.text = userName as? String
            
        }
        if let email = GlobalDefault.value(forKey: "userEmail") {
            
            txtEmail.text = email as? String
            
        }
        if let contact = GlobalDefault.value(forKey: "userContactNumber") {
            
            txtContact.text = contact as? String
            
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
           
            self.passwordView.isHidden = true
            
        case 1:
            
            self.passwordView.isHidden = false
            
        default:
            break
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        getCountryList()
        
    }
    
    
    func getCountryList() {
        
        if  Connectivity.isConnectedToInternet {
            
         
            apiHandler.getCountryList { (isSuccess, message) in
                
                if self.apiHandler.countryData?.resultData?.count ?? 0 > 0 {
                    
                    self.countryTableView.delegate = self
                    self.countryTableView.dataSource = self
                    self.countryTableView.reloadData()
                    
                }
               
               
                
            }
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
    
    
    
    }
    
    
    
    
    
    @IBAction func EditPressed(_ sender: apButton) {
        
        if  sender.tag == 0{
            
            self.txtName.isEnabled = true
            self.txtContact.isEnabled = true
            self.txtName.becomeFirstResponder()
            self.btnEdit.setTitle("SUBMIT", for: .normal)
            sender.tag = 1
            
        }else{
            
           // self.btnEdit.setTitle("EDIT", for: .normal)
            self.txtName.resignFirstResponder()
            self.txtContact.resignFirstResponder()
            sender.tag = 0
            updateProfile()
            
            
        }
        
        
        
    }
    
    
    
    @IBAction func passwordEditPressed(_ sender: UIButton) {
        
        if  sender.tag == 100{
            
            self.txtCurrentPassword.isEnabled = true
            self.txtConfirmpassword.isEnabled = true
            self.txtNewPassword.isEnabled = true
            self.txtCurrentPassword.becomeFirstResponder()
            self.btnPasswordEdit.setTitle("SUBMIT", for: .normal)
            sender.tag = 101
            
        }else{
            
            //self.btnPasswordEdit.setTitle("EDIT", for: .normal)
            self.txtCurrentPassword.resignFirstResponder()
            self.txtNewPassword.resignFirstResponder()
            self.txtConfirmpassword.resignFirstResponder()
            sender.tag = 100
            updatePassword()
            
            
        }
        
        
        
        
    }
    
    
    
    
    func updatePassword(){
        
        if  Connectivity.isConnectedToInternet {
            
            if (txtNewPassword.text?.isEmpty)! || (txtConfirmpassword.text?.isEmpty)! || (txtCurrentPassword.text?.isEmpty)! {
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }else if(txtNewPassword.text != txtConfirmpassword.text ){
                
                Alertift.alert(title: AppTitle, message:passwordConfirmation)
                    .action(.default("OK"))
                    .show(on: self)
                
            }
            else{
                
                let params = [
                    "currentPassword": txtCurrentPassword.text!,
                    "newPassword": txtNewPassword.text!
                   
                    
                    
                    ] as [String : Any]
                
                
                
                SVProgressHUD.show()
                
                apiHandler.updatePassword(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                        GlobalDefault.set(self.txtName.text, forKey: "name")
                        GlobalDefault.set(self.contryName, forKey: "countryName")
                        GlobalDefault.set(self.txtContact.text, forKey: "userContactNumber")
                        
                        Alertift.alert(title: AppTitle, message: message)
                            .action(.default("OK"))
                            .show(on: self)
                        
                        
                    }
                    else{
                        
                        
                        
                        Alertift.alert(title: AppTitle, message: "Failed to update")
                            .action(.default("OK"))
                            .show(on: self)
                        
                        
                        
                        
                        
                    }
                    
                    //print(self.LoginModel.userProfile?.message as Any)
                    
                }
                
                
                
            }
            
            
            
        }
            
            
        else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
        
        
    }
    
    
    func updateProfile(){
        
        if  Connectivity.isConnectedToInternet {
            
            if (txtName.text?.isEmpty)! || (txtContact.text?.isEmpty)! {
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }
            else{
                
                let params = [
                    "name": txtName.text!,
                    "username": txtUserName.text!,
                    "countrySelection": self.countryId,
                    "contact": txtContact.text!,
                    "address":""
                    
                    
                    ] as [String : Any]
                
                
                
                SVProgressHUD.show()
                
                apiHandler.updateProfile(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                       GlobalDefault.set(self.txtName.text, forKey: "name")
                       GlobalDefault.set(self.contryName, forKey: "countryName")
                       GlobalDefault.set(self.txtContact.text, forKey: "userContactNumber")
                        
                        Alertift.alert(title: AppTitle, message: message)
                            .action(.default("OK"))
                            .show(on: self)
                            
                     
                        }
                        else{
                            
                        

                                Alertift.alert(title: AppTitle, message: "Failed to update")
                                    .action(.default("OK"))
                                    .show(on: self)

                        
                        
                            
                            
                        }
                        
                        //print(self.LoginModel.userProfile?.message as Any)
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
            
            else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
        
        
    }
    
    
    
    
    
    @IBAction func coutryPressed(_ sender: Any) {
        
    self.countryTableView.isHidden = false
        
    }
    
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileSettingsViewController: UITableViewDelegate ,UITableViewDataSource {
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return (self.apiHandler.countryData?.resultData!.count)!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = countryTableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as? countryCell
        
        if let info = self.apiHandler.countryData?.resultData![indexPath.row]
        
        {
            
         cell!.countryTitle.text = info.name
            
        }

        
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let info = self.apiHandler.countryData?.resultData?[indexPath.row] {
            
           
               self.txtCountry.text = info.name
            
                self.countryId = info.id
                self.contryName = info.name
            
            self.countryTableView.isHidden = true
            
        }
        
        
    
    }
    
    
}
