//
//  PaymentViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/9/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD

class PaymentViewController: BaseViewController {
    
    
    
    
    var userId:String?
    var purposeStr:String?
    var memberShipId:String?
    
    
    @IBOutlet weak var paymentWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard let usrId = userId, let purposeString = purposeStr, let membershipStr = memberShipId  else {
        return
        }
        
        paymentWebView.delegate = self
        
        goForPayment(userId: usrId, membershipId: membershipStr, puropose: purposeString)

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    func goForPayment(userId:String,membershipId:String,puropose:String) {
         if  Connectivity.isConnectedToInternet {
            
            
    


            
            
            let urlstring = paymentProcess
            let url = URL(string: urlstring)!
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let query :[String:String] = ["user_id":userId,"membership_id":membershipId,"purpose":puropose]
            let baseurl = URL(string:urlstring)!
            let postString = (baseurl.withQueries(query)!).query
            request.httpBody = postString?.data(using: .utf8)
            paymentWebView.loadRequest(request as URLRequest)
            
             SVProgressHUD.show()
            
           // paymentWebView.load(request, mimeType: <#String#>, textEncodingName: <#String#>, baseURL: <#URL#>)
            
//            let url = URL (string:paymentProcess)
//            let request = NSMutableURLRequest(url: url!)
//            request.httpMethod = "POST"
//            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//
//            let post: String = "user_id="\("")"&membership_id="\("")"
//            let postData: Data = post.data(using: String.Encoding.ascii, allowLossyConversion: true)!
//
//            request.httpBody = postData
//            paymentWebView.load(request)
            
            
         }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
            
        }
    }
    

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentViewController:UIWebViewDelegate {
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
         SVProgressHUD.show()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        SVProgressHUD.dismiss()
        
    }
    
    
    
}
