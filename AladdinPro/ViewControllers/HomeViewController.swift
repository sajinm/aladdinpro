//
//  HomeViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 3/21/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD
import HAActionSheet

class HomeViewController: BaseViewController {
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    @IBOutlet weak var btnCancelCategory: UIButton!
    
    @IBOutlet weak var btnMore: UIButton!
    
    @IBOutlet weak var emptyBanner: UIImageView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var calendarParentView: UIView!
    @IBOutlet weak var calendarPicker: UIDatePicker!
    @IBOutlet weak var catParentView: UIView!
    
    @IBOutlet weak var btnCategory: UIButton!
    
   
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var filterView: UIView!
    // @IBOutlet weak var filterStackConstraintConstant: NSLayoutConstraint!
    
    @IBOutlet weak var filterViewConstraint: NSLayoutConstraint!
    var catergoryID:String = "0"
    var dataArray:[documentModel]?
    var sortCopyArray:[documentModel]?
    var companyArray:[CmpDataModel]?
    var companyNames:[String]? = []
    var dataCatArray:[categoryData]?
    
    let getDocument = ResponseHandler()
    var cntCompnayId:String?
    var dateStr:String? = ""
    var compnayID:String = ""
    
    var rootVC : UIViewController?
    
    
    var isCategroyLoaded:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

       //initialSetup()
  
        // Do any additional setup after loading the view.
    }
    
    func initialSetup() {
        
        self.filterView.isHidden = true
        self.filterViewConstraint.constant = 0
        self.emptyLabel.isHidden = true
        self.emptyBanner.isHidden = true
       // catParentView.isHidden = true
        homeTableView.isHidden = true
      //  calendarParentView.isHidden = true
        
//        self.calendarPicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)

        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let day = components.day





        dateStr = dateStr?.formatDate(date: date)
        
        if let companyId = GlobalDefault.value(forKey: "companyId"){
            
            
            cntCompnayId = companyId as? String
            self.compnayID = cntCompnayId!
            getDocuments(compnayID: cntCompnayId!,dateStr:dateStr! )
            getPackageDetails()
        }
        

        
        var dtString:String = ""
        dtString = dtString.formatWithMonth(date: date)
//        btnCalendar.setTitle(dtString, for: .normal)
//        btnCategory.setTitle("All Categories", for: .normal)
       // self.getComapnies()

       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first
        if touch?.view == self.filterView
        {
            self.filterView.isHidden = true
            self.btnFilter.tag = 0
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        initialSetup()
        
    }
    
    @IBAction func morePressed(_ sender: UIButton) {

        if sender.tag == 0{
            
            self.filterView.isHidden = false
            UIView.animate(withDuration: 0.5,
                                        delay: 0.0,
                                        options: [.transitionCurlDown],
                                        animations: {
                                            
                                            self.filterViewConstraint.constant  += self.view.bounds.height
                                     
                                          self.view.layoutIfNeeded()
                                        }, completion: nil)
            
            
            sender.tag = 1
        }else{
            
            UIView.animate(withDuration: 0.5,
                                               delay: 0.0,
                                               options: [.curveEaseOut],
                                               animations: {
                                                
                                                  self.filterViewConstraint.constant  -= self.view
                                                    .bounds.height
                                                  self.filterView.isHidden = true
                                                 self.view.layoutIfNeeded()
                                               }, completion: nil)
           

            sender.tag = 0
        }
        
       
        
    }
    
    
    
    func getPackageDetails(){
        
        
        if  Connectivity.isConnectedToInternet {
            
           // SVProgressHUD.show()
            
            getDocument.FetchPackages { (isSuccess, Message) in
                
              
                
                if isSuccess{
                    
            
                    
                    
                    //let data = ["a": 0, "b": 42]
                    let filtered = self.getDocument.packageData!.resultData.filter { $0.isCurrent == true}
                    
                    if filtered.count > 0{
                        
                        let packageString = filtered[0].name
                       
                      
                        GlobalDefault.set(packageString, forKey: "currentPlan")
                        
    
                    }
                   
                    
                    
                    
                    
                }
                
                //  SVProgressHUD.dismiss()
                
            }
            
         }else {
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        

        
        
    }
    
    
    @IBAction func filterSelectionPressed(_ sender: UIButton) {
        
        if self.sortCopyArray?.count ?? 0 > 0 {
        
        let toSort = self.sortCopyArray
        
        switch sender.tag {
        case 1:
            
            let sortArray = toSort?.filter { $0.severity == "Expired" || $0.days_left == 0}
             
            self.dataArray = sortArray
            
            if self.dataArray?.count == 0{
                
                self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                
            }
                
        
            
             self.homeTableView.delegate = self
             self.homeTableView.dataSource = self
             self.homeTableView.reloadData()
             self.homeTableView.isHidden = false
            
            
                
               
                
       
            
            self.filterView.isHidden = true
            self.btnFilter.tag = 0
            
        case 2:
          
            
            let sortArray = toSort?.filter { $0.severity == "Critical" && $0.days_left != 0 }
                        
                       self.dataArray = sortArray
                       
                       if self.dataArray?.count == 0{
                              
                              self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                              
                          }
                                       
                                    self.homeTableView.delegate = self
                                    self.homeTableView.dataSource = self
                                    self.homeTableView.reloadData()
                                    self.homeTableView.isHidden = false
                                   
                                   
                       
                       self.filterView.isHidden = true
                       self.btnFilter.tag = 0
            
        case 3:
            
            let sortArray = toSort?.filter { $0.severity == "Expiring" }
             
            self.dataArray = sortArray
            
            if self.dataArray?.count == 0{
                           
                           self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                           
                       }
            
                        
                         self.homeTableView.delegate = self
                         self.homeTableView.dataSource = self
                         self.homeTableView.reloadData()
                         self.homeTableView.isHidden = false
                        
                        
            
            self.filterView.isHidden = true
            self.btnFilter.tag = 0
       
            
        case 4:
        
        
         
        self.dataArray = self.sortCopyArray
        
        if self.dataArray?.count == 0{
                       
                       self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                       
                   }
                        
                     self.homeTableView.delegate = self
                     self.homeTableView.dataSource = self
                     self.homeTableView.reloadData()
                     self.homeTableView.isHidden = false
                    
                
        
        self.filterView.isHidden = true
        self.btnFilter.tag = 0
            
            
        default:
            break
        }
        
        
        }
    }
    

    
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        
        var newDate:String = ""
    
        newDate = newDate.formatWithMonth(date: calendarPicker.date)
        btnCalendar.setTitle(newDate, for: .normal)
        
        self.dateStr = newDate.formatDate(date: calendarPicker.date)
    }
    
    
    @IBAction func addNewPressed(_ sender: Any) {
        
        //if self.catergoryID != "0" {
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "catListVC") as? CategoryViewController {
            viewController.isAddNewReminder = true
                   if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
      //  }
        
    }
    
    @IBAction func categoryCancelPressed(_ sender: Any) {
        catParentView.isHidden = true
        
    }
    
    
    
    @IBAction func categoryPressed(_ sender: Any) {
        
        catParentView.isHidden = false
    }
    
    
    
    func getDocumentForSelection(compnayID:String,docId:String) {
        
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            let params = [
                
                "docTypeSelection": docId,
                "company_id": compnayID
               
                
                ] as [String : Any]
            
            SVProgressHUD.show()
            
            getDocument.getSelectedCatDocs(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess {
                    
                    if self.getDocument.selectedData!.resultCode == 1{
                        
                        if (self.getDocument.selectedData?.resultData?.data.count)! > 0{
                            
                            if self.dataArray != nil{
                                
                                self.dataArray?.removeAll()
                            }
                            
                            self.dataArray = self.getDocument.selectedData?.resultData?.data
                            
                        print(self.dataArray!)
                            
                            self.homeTableView.delegate = self
                            self.homeTableView.dataSource = self
                            self.homeTableView.reloadData()
                            self.homeTableView.isHidden = false
                            

                            
                        }
                        else{
                            
                            if self.dataArray != nil{
                                
                                self.dataArray?.removeAll()
                                 self.homeTableView.reloadData()
                                
                                Alertift.alert(title: AppTitle, message: nodataFound)
                                    .action(.default("OK"))
                                    .show(on: self)
                            }
                            
                            
                        }
                        
                        
                        
                    }
                }
            }
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
    }
    
    
    
    
    
    
    
    func getDocuments(compnayID:String,dateStr:String) {
        
        
        
        
          if  Connectivity.isConnectedToInternet {
            
            let params = [
                
                "date": dateStr,
                "company_id": compnayID,
                "days": "300"
            
                ] as [String : Any]
            
            SVProgressHUD.show()
            
            getDocument.getDocumentData(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess {
                
                if self.getDocument.documentData!.resultCode == 1{
                    
                    if (self.getDocument.documentData?.resultData?.data.count)! > 0{
                        
                        let data = self.getDocument.documentData?.resultData?.data
                            
                  
                        
                        self.dataArray = data?.sorted(by: { $0.days_left < $1.days_left })
                        self.sortCopyArray = self.dataArray

                        
                       
                        self.homeTableView.delegate = self
                        self.homeTableView.dataSource = self
                        self.homeTableView.reloadData()
                        self.homeTableView.isHidden = false
                        
//                        if let companyId = GlobalDefault.value(forKey: "companyId"){
//
//                            let cntCompnayId = companyId as? String
//
//
//                            self.getCategories(companyId: cntCompnayId!)
//
//                        }
                        
                    }
                    else{
                        
                        self.emptyLabel.isHidden = false
                        self.emptyBanner.isHidden = false
                        
//                        Alertift.alert(title: AppTitle, message: "Document list seems to be empty")
//                            .action(.default("OK"))
//                            .show(on: self)
                        
                        
                    }
                    
                   
                    
                }
                else{
                    
                    if message == "token_expired"{
                        
                       
                        
                            
                            Alertift.alert(title: AppTitle, message: "Session timeout. Please login.")
                                                   .action(.default("OK"))
                                                   .show(on: self)
                        
                      
                              
//                        DispatchQueue.main.async {
//
//                            self.performSegue(withIdentifier: "HomeToLogin", sender: Any?.self)
//
//                        }
//
                            
                            
                  
                        
                        
                        
                    }else{
                        
                        Alertift.alert(title: AppTitle, message: message)
                                                                       .action(.default("OK"))
                                                                       .show(on: self)
                        
                    }
                    
                    
                   
                    
//                     self.rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialNavigation") as! UINavigationController
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = self.rootVC
                    
                    }
                }
                
                
                
                
            }
            
          }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
    
    }

    
    
    func getCategories(companyId:String){
        
        
        SVProgressHUD.show()
        
        let params = [
            
            
            "company_id": companyId,
            "noOfRows":"100"
            
            
            ] as [String : Any]
        
         self.getDocument.getCategoryData(withParameter: params) { (isSuccess, Message) in
            
             SVProgressHUD.dismiss()
            
            if isSuccess {
                
                self.dataCatArray = self.getDocument.categoryData?.resultData?.data
                
                let allCat = categoryData(id:"0",name:"All Categories")
                
                self.dataCatArray?.append(allCat)
                
                
                
                //print(self.dataCatArray!)
               // self.categoryTableView.isHidden = false
                self.categoryTableView.delegate = self
                self.categoryTableView.dataSource = self
                self.categoryTableView.reloadData()
                
            }
            
            
        }
        
    }
    
    
    
 
    
    
    @IBAction func calendarPressed(_ sender: Any) {
        calendarParentView.isHidden = false
    }
    
    
    @IBAction func calendarDonePressed(_ sender: Any) {
        
         calendarParentView.isHidden = true
    }
    

}
extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == homeTableView{
            
            
            return (self.dataArray?.count)!
        }
        else{
            
            return (self.dataCatArray?.count)!
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.homeTableView {
            
        
        
        let cell = self.homeTableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath) as! ReminderCell
        
        let infoData = self.dataArray![indexPath.row]
        
     
        
       cell.circleView.backgroundColor = Utils.setType(type:infoData.severity)
       cell.lblTitle.text = infoData.description
       cell.lblDayCount.text = "\(infoData.days_left)"
            if infoData.days_left == 0 || infoData.days_left < 0{
                
                cell.circleView.backgroundColor = Utils.setType(type:"Expired")
                
            }
            else{
                
                cell.circleView.backgroundColor = Utils.setType(type:infoData.severity)
                
            }
       cell.lblExpiryDate.text = infoData.expiry_date
       cell.lblCategory.text = infoData.document_type
        
        return cell
    }
        else{
            
            let cell = self.categoryTableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath) as! ReminderCell
            
            let infoData = self.dataCatArray![indexPath.row]
            
            cell.lblTitle.text = infoData.name
            
            return cell
            
            
            
        }
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == self.categoryTableView{
            
             let infoData = dataCatArray![indexPath.row]
            
            let catTitle = infoData.name
            self.catergoryID = infoData.id
            btnCategory.setTitle(catTitle, for: .normal)
            
            self.getDocumentForSelection(compnayID: self.compnayID, docId: self.catergoryID)
            self.catParentView.isHidden = true
            
           
            
           
            
            
        }
        else{
            
            
             let infoData = dataArray![indexPath.row]
            
  
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditReminderVC") as? EditReminderViewController {
               // viewController.categoryId = self.catergoryID
                if let navigator = navigationController {
                    
                    
                    viewController.docId = infoData.id
                    viewController.documentNumber = infoData.document_no
                    viewController.dueDate = infoData.expiry_date
                    viewController.descText = infoData.description
                    viewController.companyId = infoData.company_id
                    viewController.docTypeId = infoData.document_type_id
                    viewController.attachUrl = infoData.file_name
                    navigator.pushViewController(viewController, animated: true)
                    
                    
                   
                   

                }
            }
            
            
            
            
            
        }
        
    }
    
    
}


