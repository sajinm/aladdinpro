//
//  AboutViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 30/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var listTableView: UITableView!
    
    
    var menuArray = ["Contact","Terms of Service","Privacy Policy"]
    var urlArray = [contact,terms,privacy]
    override func viewDidLoad() {
        super.viewDidLoad()
        
       initialLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func initialLoad(){
        
         self.listTableView.tableFooterView = UIView()
         self.listTableView.delegate = self
         self.listTableView.dataSource = self
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    

  

}

extension AboutViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = self.listTableView.dequeueReusableCell(withIdentifier: "aboutListCell", for: indexPath) as! MenuCell
        cell.menuTitle.text = menuArray[indexPath.row]
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebVC") as? WebViewViewController {
                           
                                 viewController.headerText = menuArray[indexPath.row]
            viewController.loadUrl = urlArray[indexPath.row]
                              if let navigator = navigationController {
                                  navigator.pushViewController(viewController, animated: true)
                              }
                          }
        
        
    }
    
    
}
