//
//  AddNewCategoryViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/2/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
import Alertift

class AddNewCategoryViewController: BaseViewController {
    
    @IBOutlet weak var txtCategoryName: HoshiTextField!
    @IBOutlet weak var txtDueDays: HoshiTextField!
    @IBOutlet weak var txtCriticalDays: HoshiTextField!
    
    var newCategory = ResponseHandler()

    override func viewDidLoad() {
        super.viewDidLoad()

        setInitialView()
        // Do any additional setup after loading the view.
    }
    
    func setInitialView(){
    
       // self.txtCategoryName.becomeFirstResponder()
        
    }
    
    func addNewCategory(companyId:String,categoryName:String,dueDays:String,criticalDays:String) {
    
        
        if  Connectivity.isConnectedToInternet {
            
            if ((txtCategoryName.text?.count)! < 3) {
                
                Alertift.alert(title: AppTitle, message:valideCategoryName)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }else{
                
              //  SVProgressHUD.show()
                
                let params = [
                    
                    
                    "docTypeName_input": categoryName,
                    "company_id":companyId,
                    "expBefore":dueDays,
                    "criticalBefore":criticalDays,
                    "alertEnabled":"yes",
                    "userGroupSelection":[]
                    
                    
            
                    ] as [String : Any]
                
                self.newCategory.addNewCategory(withParameter: params) { (isSuccess, message) in
                    
                  //   SVProgressHUD.dismiss()
                    if isSuccess {
                       
                        self.navigationController?.popViewController(animated: true)
                        
                       
                      //  self.setToast(message:message,onController:self.navigationController!)
//                        Alertift.alert(title: AppTitle, message: message)
//                            .action(.default("OK"))
//                            .show(on: self)
                        
                    }
                    
                }
                
                
                
                
            }
        
        }else{
            
            
           // self.setToast(message:noInternrt,onController:self.navigationController!)
            
//            Alertift.alert(title: AppTitle, message: noInternrt)
//                .action(.default("OK"))
//                .show(on: self)
            
            
        }
        
    }
    
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        
        if let companyId = GlobalDefault.value(forKey: "companyId"){
            
            let cmpId = companyId as? String
            
            self.addNewCategory(companyId: cmpId!, categoryName: txtCategoryName.text!, dueDays: txtDueDays.text!, criticalDays: txtCriticalDays.text!)
            
        }
        
        
       
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    

   
}
