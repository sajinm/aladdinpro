//
//  LoginViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 2/6/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var btnSignUp: apButton!
    
    @IBOutlet weak var txtMobEmail: ImageTextField!
    @IBOutlet weak var txtPassword: ImageTextField!
    
    
    
    let LoginModel = ResponseHandler()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpinitialView()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "loginToPackage", sender: Any?.self)
        
    }
    
    
    
    
    func  setUpinitialView()  {
        
        let attrStr = NSMutableAttributedString(string: "New here? Sign Up")
        attrStr.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1611390114, green: 0.7553957105, blue: 0.5481526852, alpha: 1), range: NSRange(location: 10, length: 7))
      //  btnSignUp.setAttributedTitle(attrStr, for: .normal)
    }

    
    
    
    @IBAction func forgotPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "toForgotPwd", sender: Any?.self)
        
        
    }
    
    
    @IBAction func signInPressed(_ sender: Any) {
        
        txtPassword.resignFirstResponder()
        txtMobEmail.resignFirstResponder()
        
        if  Connectivity.isConnectedToInternet {

            if (txtMobEmail.text?.isEmpty)! ||  (txtPassword.text?.isEmpty)!  {

                Alertift.alert(title: AppTitle, message:validUsername)
                    .action(.default("OK"))
                    .show(on: self)

            
            }
            else{
                
                let params = [
                    "username": txtMobEmail.text!,
                    "password": txtPassword.text!,
                  
                    ] as [String : Any]
                
                   SVProgressHUD.show()
                
                   LoginModel.doLogin(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                        if self.LoginModel.userProfile?.resultCode == 1{
                            
                        
                            
                             GlobalDefault.set(self.LoginModel.userProfile?.resultData?.auth_token, forKey: "userToken")
                             GlobalDefault.set(self.LoginModel.userProfile?.resultData?.default_company, forKey: "companyId")
                            
                            
                          
                            if let emailStr = self.LoginModel.userProfile?.resultData?.email{
                                
                                 GlobalDefault.set(emailStr, forKey: "userEmail")
                                
                            }
                            
                            if let nameStr = self.LoginModel.userProfile?.resultData?.name{
                                
                                GlobalDefault.set(nameStr, forKey: "name")
                                
                            }
                            
                            if let countryStr = self.LoginModel.userProfile?.resultData?.country_name{
                                
                                GlobalDefault.set(countryStr, forKey: "countryName")
                                
                            }

                          
                            if let contactStr = self.LoginModel.userProfile?.resultData?.contact{
                                
                                GlobalDefault.set(contactStr, forKey: "userContactNumber")
                                
                            }
                            
                            if let usrId = self.LoginModel.userProfile?.resultData?.id{
                                
                                GlobalDefault.set(usrId, forKey: "userID")
                                
                            }
                            
                            if let membershipId = self.LoginModel.userProfile?.resultData?.membership_id{
                                
                                GlobalDefault.set(membershipId, forKey: "membdershipID")
                                
                            }
                            
                            
                            if let userNameStr = self.LoginModel.userProfile?.resultData?.username{
                                
                                GlobalDefault.set(userNameStr, forKey: "userName")
                                
                            }
                            Switcher.updateRootVC()
                            
                            //self.performSegue(withIdentifier: "loginToHomeView", sender: Any?.self)
                            
                            
                        }
                        else{
                            
                            if let msgStr = self.LoginModel.userProfile?.message{
                                
                                Alertift.alert(title: AppTitle, message: msgStr)
                                    .action(.default("OK"))
                                    .show(on: self)
                                
                            }
                            
                            
                            
                        }
                       
                        //print(self.LoginModel.userProfile?.message as Any)
                        
                    }
                    
                    
                    
                }
                
            
            
            }
        
        
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
    
    
        
        //performSegue(withIdentifier: "loginToHomeView", sender: Any?.self)
    }
    

}
