//
//  MoreViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/7/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift
import HAActionSheet

class MoreViewController: BaseViewController {
    
    
     let getDocument = ResponseHandler()
    var companyArray:[CmpDataModel]?
    var companyNames:[String]? = []
     var packageString:String? = ""
     var getPackage = ResponseHandler()
    
    
    @IBOutlet weak var lblPackage: UILabel!
    
       let imagePicker = UIImagePickerController()
    
    enum StorageType {
           case userDefaults
           case fileSystem
       }
    
    @IBOutlet weak var menuTableView: UITableView!{
        didSet {
            menuTableView.tableFooterView = UIView(frame: .zero)
        }
    }
   
    var userPic:UIImage?
    
    
    var menuTitleArray = ["","Account","Billing","PDF Settings","Switch Company","Help","About","Log Out"]
     var menuiconArray = ["","ic_menu_user","ic_package","ic_pdf_size","ic_company_gray","ic_help","ic_about","ic_logout"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableView.delegate = self
        menuTableView.dataSource = self
        self.imagePicker.delegate = self

        //self.getPackageDetails()
        self.getComapnies()
        
        
        //self.menuTableView.footerView
        // Do any additional setup after loading the view.
    }
    
    
    
    

    
    func switchCompany(){
        
        
        if self.companyNames!.count > 1 {
            
             self.tabBarController?.tabBar.isHidden = true
            
            let view = HAActionSheet(fromView: self.view, sourceData: self.companyNames ?? [""])
            view.title = ""
            view.message = "Select Company"
            view.show { (canceled, index) in
                if !canceled {
                    
                    let cmpInfo = self.companyArray![index!]
                    GlobalDefault.set(cmpInfo.id, forKey: "companyId")
                    GlobalDefault.set(cmpInfo.name, forKey: "companyName")
                    
                    self.menuTableView.reloadData()
                    
                     self.tabBarController?.tabBar.isHidden = false
                    
                }
                
                else{
                    
                    self.tabBarController?.tabBar.isHidden = false
                }

                
            }
            
            
        }else if (self.companyNames!.count == 0){
            
            
            self.setToast(message:"Multiple companies not found.",view:self.view,onController:self.navigationController!)

            
            
            return
            
        }
        
        
        
    }
    
    
   
    
    func getPackageDetails(){
        
        
        if  Connectivity.isConnectedToInternet {
            
            SVProgressHUD.show()
            
            getPackage.FetchPackages { (isSuccess, Message) in
                
              
                
                if isSuccess{
                    
            
                    
                    
                    //let data = ["a": 0, "b": 42]
                    let filtered = self.getPackage.packageData!.resultData.filter { $0.isCurrent == true}
                    
                    if filtered.count > 0{
                        
                        self.packageString = filtered[0].name
                       
                       // print(self.packageString!)
                        
                        GlobalDefault.set(self.packageString, forKey: "currentPlan")
                        
                        
                       
                        
                    }
                   
                    
                    
                    
                    
                }
                
                  SVProgressHUD.dismiss()
                
            }
            
         }else {
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        

        
        
    }
    
    
    @objc
    private func display() -> UIImage {
        
        var userPic = UIImage()
      
            if let savedImage = self.retrieveImage(forKey: "userPic",
                                                   inStorageType: .fileSystem) {
                userPic = savedImage
            }
        
        return userPic
         
    }
    
    private func retrieveImage(forKey key: String,
                                  inStorageType storageType: StorageType) -> UIImage? {
           switch storageType {
           case .fileSystem:
               if let filePath = self.filePath(forKey: key),
                   let fileData = FileManager.default.contents(atPath: filePath.path),
                   let image = UIImage(data: fileData) {
                   return image
               }
           case .userDefaults:
               if let imageData = UserDefaults.standard.object(forKey: key) as? Data,
                   let image = UIImage(data: imageData) {
                   return image
               }
           }
           
           return nil
       }
    
    func getComapnies() {
        
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            let params = [
                
                "page": "0",
                "noOfRows": "0"
                
                
                ] as [String : Any]
            
            SVProgressHUD.show()
            
            getDocument.getCompanyList(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess {
                    
                    if self.getDocument.companyData!.resultCode == 1{
                        
                        if (self.getDocument.companyData?.resultData?.data.count)! > 0{
                            
                            self.companyArray = self.getDocument.companyData?.resultData?.data
                            
                            if self.companyArray?.count ?? 0 > 0{
                               
                                for data in self.companyArray!{
                                                            
                                    if data.is_default == "yes"{
                                        
                                         GlobalDefault.set(data.name, forKey: "companyName")
                                        DispatchQueue.main.async {
                                            self.menuTableView.reloadData()
                                        }
                                        
                                    }
                                                               
                                                           }
                                
                            }
                            
                           
                            
                            //  print(self.companyArray!)
                            
                            if self.companyArray!.count > 0{
                                
                                if self.companyNames != nil{
                                    
                                    self.companyNames?.removeAll()
                                }
                                
                                for (name) in self.companyArray! {
                                    
                                    let nameStr = name.name as String
                                    self.companyNames?.append(nameStr)
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
                
                
                
                
            }
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
    }

    
    

    
    private func filePath(forKey key: String) -> URL? {
           let fileManager = FileManager.default
           guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                    in: .userDomainMask).first else {
                                                       return nil
           }
           
           return documentURL.appendingPathComponent(key + ".png")
       }
    
    @objc
    private func save(userPic:UIImage) {
          
               DispatchQueue.global(qos: .background).async {
                   self.store(image: userPic,
                              forKey: "userPic",
                              withStorageType: .userDefaults)
                
               }
           
       }
    
    
    private func store(image: UIImage,
                       forKey key: String,
                       withStorageType storageType: StorageType) {
        if let pngRepresentation = image.pngData() {
            switch storageType {
            case .fileSystem:
                if let filePath = filePath(forKey: key) {
                    do {
                        try pngRepresentation.write(to: filePath,
                                                    options: .atomic)
                    } catch let err {
                        print("Saving results in error: ", err)
                    }
                }
            case .userDefaults:
                UserDefaults.standard.set(pngRepresentation,
                                          forKey: key)
            }
        }
    }

}
extension MoreViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitleArray.count
    }
    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0{
           
            return 110
            
        }
        
        return 50
        
        
         //or whatever you need
    }
    
    
    
    
   
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if tableView == menuTableView {
            
            self.menuTableView.separatorStyle = .none

        if indexPath.row == 0 {
            
             self.menuTableView.separatorStyle = .none
            let cell = menuTableView.dequeueReusableCell(withIdentifier: "profileMenuCell", for: indexPath) as! MenuCell

              cell.delegate = self
            
            
            if GlobalDefault.value(forKey: "currentPlan") != nil{
                
                
                cell.lblPackage.text = GlobalDefault.value(forKey: "currentPlan") as? String
                
            }else{
                
                if let package = self.packageString {
                               
                     cell.lblPackage.text = package
                               
                           }
                           
                
            }
            
           
            

            if GlobalDefault.value(forKey: "name") != nil {

                let  userName = GlobalDefault.value(forKey: "name") as? String

                cell.menuTitle.text = userName

            }else{

                cell.menuTitle.text = "GUEST"

            }
            
            
            if GlobalDefault.value(forKey: "userName") != nil {

                let  userName = GlobalDefault.value(forKey: "userName") as? String

                cell.lblUsername.text = userName

            }else{

                cell.menuTitle.text = ""

            }
            
       
            
           
            
//
//                if let image = self.userPic{
//
//                                cell.iconImageView.image = UIImage(named: "btn_remove") //image
//                           }
       
            
           
            
            
//            if let savedImage = self.retrieveImage(forKey: "userPic",
//                                                              inStorageType: .userDefaults) {
//                cell.iconImageView.layer.cornerRadius = cell.iconImageView.frame.width/2
//                cell.clipsToBounds = true
//                cell.iconImageView.contentMode = .scaleAspectFill
//                cell.iconImageView.image = savedImage
//            }else{
//
//                if let image = self.userPic{
//
//                     cell.iconImageView.image = image
//                }else{
//
//                     cell.iconImageView.image = UIImage(named:"avathar")
//
//                }
//
//
//
//            }

          

        } else {

              self.menuTableView.separatorStyle = .singleLine
            let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
            cell.menuTitle.text = menuTitleArray[indexPath.row]
            cell.iconImageView.image = UIImage(named: menuiconArray[indexPath.row])
            
            if indexPath.row == 4 {
                if GlobalDefault.value(forKey: "companyName") != nil{
                    
                    cell.lblUsername.text = GlobalDefault.value(forKey: "companyName") as? String
                }
               //
                
            }
            return cell


        }

        }

       let cell = UITableViewCell()
        return cell

        }
 
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0{
            
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebVC") as? WebViewViewController {
                           
                                  viewController.headerText = pricing
                                 viewController.loadUrl = pricingUrl
                              if let navigator = navigationController {
                                  navigator.pushViewController(viewController, animated: true)
                              }
                          }
            
            
        }
        
        
        
        if indexPath.row == 1{
            
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileSettingVC") as? ProfileSettingsViewController {
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
           
        }
        
        if indexPath.row == 2{
            
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BillingVC") as? PckageBillingViewController {
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
            
        }
        
        
        if indexPath.row == 3{
            
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pdfSettingVC") as? PDFSettingViewController {
                               
                                  if let navigator = navigationController {
                                      navigator.pushViewController(viewController, animated: true)
                                  }
                              }
            
        }
        
        if indexPath.row == 5{
                   
                   if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebVC") as? WebViewViewController {
                    
                           viewController.headerText = help
                          viewController.loadUrl = supportUrl
                       if let navigator = navigationController {
                           navigator.pushViewController(viewController, animated: true)
                       }
                   }
                   
               }
        
        
        
         if indexPath.row == 4{
            
            self.switchCompany()
           
        }
        
        
        if indexPath.row == 6{
                   if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "aboutVC") as? AboutViewController {
                    
                    
                                         if let navigator = navigationController {
                                             navigator.pushViewController(viewController, animated: true)
                                         }
                                     }
                  
               }
               
        
        if indexPath.row == 7{
            
            GlobalDefault.set(nil, forKey: "userToken")
            
           
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginViewController {
                if let navigator = navigationController {
                    
                    self.hidesBottomBarWhenPushed = true
                    navigator.pushViewController(viewController, animated: true)
                }
            }
            
            
            
        }
        
       
        
       
    }
    
    
    
    
}
extension MoreViewController:menuCellDelgate{
    func setProfilePic() {
        
        
        self.imagePicker.allowsEditing = false
                           self.imagePicker.sourceType = .photoLibrary
                                  
                           self.present(self.imagePicker, animated: true, completion: nil)
                           
        
        /*
         self.tabBarController?.tabBar.isHidden = true
        let view = HAActionSheet(fromView: self.view, sourceData: ["Choose Photo","Take Photo"] )
        view.title = ""
        view.message = "Set Profile Photo"
        view.show { (canceled, index) in
            if !canceled {
                
                
                switch index {
                case 0:
                    
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.sourceType = .photoLibrary
                           
                    self.present(self.imagePicker, animated: true, completion: nil)
                    
                    
                case 1:
                    
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.sourceType = .camera
                                              
                    self.present(self.imagePicker, animated: true, completion: nil)
                    
                    
                default:
                    break
                }
                
               self.tabBarController?.tabBar.isHidden = false
                
            }
            
            else{
                self.tabBarController?.tabBar.isHidden = false
                
            }

            
        }
        
   */
    }
    

    
    
}

extension MoreViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{


 func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
     
     var selectedImage: UIImage?
     if let editedImage = info[.editedImage] as? UIImage {
        selectedImage = editedImage.resize(toWidth: 150)
        
       // self.save(userPic: selectedImage!)
        
    
       
    
       
        
        
        if let image = selectedImage{
                
                self.userPic = image
            }
            
           let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "profileMenuCell") as! MenuCell

          cell.userImg = selectedImage

          
            //cell.menuTitle.text =  "hihiihi"
           
               
                self.menuTableView.reloadData()
         picker.dismiss(animated: true, completion: nil)
            
        
     } else if let originalImage = info[.originalImage] as? UIImage {
        selectedImage = originalImage.resize(toWidth: 150)
        
        
       
        
       // self.save(userPic: selectedImage!)
        

      
        
        
        
            if let image = selectedImage{
                      
                      self.userPic = image
                  }
                  
            let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "profileMenuCell") as! MenuCell

                          cell.userImg = selectedImage
                         
                              self.menuTableView.reloadData()
                          
         picker.dismiss(animated: true, completion: nil)
        
        
     }
    
    
   
     
}
}
