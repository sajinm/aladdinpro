//
//  PackageViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 3/19/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit

class PackageViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var packageCollectionView: UICollectionView!
    
    var packageArray = ["packageOne","packageTwo","packageThree","packageFour"]
    var packageValue = ["0","3","4","5"]
    var currentPage = 0
    var selectedPackage = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.packageCollectionView.delegate = self
        self.packageCollectionView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
       if  segue.identifier == "packageToSingUp" {
            
             let destination = segue.destination as! SignUpViewController
            destination.selectedPackage = self.selectedPackage
            
            
        }
        
    }
    
    

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension PackageViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return packageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = self.packageCollectionView.dequeueReusableCell(withReuseIdentifier: "packageCell", for: indexPath) as! PackageCell
        
        cell.itemPic.image = UIImage(named:packageArray[indexPath.row])
       
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        selectedPackage = packageValue[indexPath.row]
        self.performSegue(withIdentifier: "packageToSingUp", sender: Any?.self)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pageControl.currentPage = self.currentPage
    }
    
    
   
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSizeWidth = self.view.frame.width 
        var itemSizeHeight = self.view.frame.height
        
        if #available(iOS 11.0, *) {
            let guide = view.safeAreaLayoutGuide
            let height = guide.layoutFrame.size.height
            
            itemSizeHeight = height - 60
            
        } else {
            // Fallback on earlier versions
        }
        
        //        let itemSizeWidth = self.view.frame.width
        //         let itemSizeHeight = self.view.frame.height
        
        
        
        return CGSize(width: itemSizeWidth, height: itemSizeHeight)
    }
    
    
    
    
}

