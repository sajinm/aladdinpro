//
//  SelectedCategoryViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/10/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD





class SelectedCategoryViewController: BaseViewController {
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    @IBOutlet weak var categoryName: UILabel!
    
    let getDocument = ResponseHandler()
    var dataArray:[documentModel]?
    var cntCompnayId:String?
    var compnayID:String = ""
    
    var dateStr:String? = ""
    var categoryID:String?
    var categoryNameStr:String? = ""
    
     var sortCopyArray:[documentModel]?
    
     @IBOutlet weak var filterViewConstraint: NSLayoutConstraint!

    @IBOutlet weak var btnFilter: UIButton!
      
    @IBOutlet weak var filterView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        initialLoad()
        // Do any additional setup after loading the view.
    }
    
    
    func initialLoad(){
        
        self.filterView.isHidden = true
        self.filterViewConstraint.constant = 0
        self.categoryName.text = categoryNameStr
        self.homeTableView.isHidden = false
        
        let date = Date()

        dateStr = dateStr?.formatDate(date: date)
        
        if let companyId = GlobalDefault.value(forKey: "companyId"){
            
            cntCompnayId = companyId as? String
           
            
            self.getDocumentForSelection(compnayID: cntCompnayId!, docId: self.categoryID!)
           
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           let touch = touches.first
           if touch?.view == self.filterView
           {
               self.filterView.isHidden = true
               self.btnFilter.tag = 0
               
           }
       }
    
    
    
    @IBAction func donePressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
       // self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func morePressed(_ sender: UIButton) {

        if sender.tag == 0{
            
            self.filterView.isHidden = false
            UIView.animate(withDuration: 0.5,
                                        delay: 0.0,
                                        options: [.transitionCurlDown],
                                        animations: {
                                            
                                            self.filterViewConstraint.constant  += self.view.bounds.height
                                     
                                          self.view.layoutIfNeeded()
                                        }, completion: nil)
            
            
            sender.tag = 1
        }else{
            
            UIView.animate(withDuration: 0.5,
                                               delay: 0.0,
                                               options: [.curveEaseOut],
                                               animations: {
                                                
                                                  self.filterViewConstraint.constant  -= self.view
                                                    .bounds.height
                                                  self.filterView.isHidden = true
                                                 self.view.layoutIfNeeded()
                                               }, completion: nil)
           

            sender.tag = 0
        }
        
       
        
    }
    
    
    
    
    @IBAction func filterSelectionPressed(_ sender: UIButton) {
        
        if self.sortCopyArray?.count ?? 0 > 0 {
        
        let toSort = self.sortCopyArray
        
        switch sender.tag {
        case 1:
            
            let sortArray = toSort?.filter { $0.severity == "Expired" || $0.days_left == 0}
             
            self.dataArray = sortArray
            
            if self.dataArray?.count == 0{
                           
                           self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                           
                       }
            
             self.homeTableView.delegate = self
             self.homeTableView.dataSource = self
             self.homeTableView.reloadData()
             self.homeTableView.isHidden = false
            
            self.filterView.isHidden = true
            self.btnFilter.tag = 0
            
        case 2:
          
            
            let sortArray = toSort?.filter { $0.severity == "Critical" && $0.days_left != 0 }
                        
                       self.dataArray = sortArray
            
            if self.dataArray?.count == 0{
                           
                           self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                           
                       }
                       
                        self.homeTableView.delegate = self
                        self.homeTableView.dataSource = self
                        self.homeTableView.reloadData()
                        self.homeTableView.isHidden = false
                       
                       self.filterView.isHidden = true
                       self.btnFilter.tag = 0
            
        case 3:
            
            let sortArray = toSort?.filter { $0.severity == "Expiring" }
             
            self.dataArray = sortArray
            
            if self.dataArray?.count == 0{
                           
                           self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                           
                       }
            
             self.homeTableView.delegate = self
             self.homeTableView.dataSource = self
             self.homeTableView.reloadData()
             self.homeTableView.isHidden = false
            
            self.filterView.isHidden = true
            self.btnFilter.tag = 0
       
            
        case 4:
        
        
         
        self.dataArray = self.sortCopyArray
        
        if self.dataArray?.count == 0{
                       
                       self.setToast(message:"Empty",view:self.view,onController:self.navigationController!)
                       
                   }
        
         self.homeTableView.delegate = self
         self.homeTableView.dataSource = self
         self.homeTableView.reloadData()
         self.homeTableView.isHidden = false
        
        self.filterView.isHidden = true
        self.btnFilter.tag = 0
            
            
        default:
            break
        }
        
        
        }
    }
    
    
    
    func getDocumentForSelection(compnayID:String,docId:String) {
        
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            let params = [
                
                "docTypeSelection": docId,
                "company_id": compnayID
                
                
                ] as [String : Any]
            
            SVProgressHUD.show()
            
            getDocument.getSelectedCatDocs(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess {
                    
                    if self.getDocument.selectedData!.resultCode == 1{
                        
                        if (self.getDocument.selectedData?.resultData?.data.count)! > 0{
                            
                            if self.dataArray != nil{
                                
                                self.dataArray?.removeAll()
                            }
                            
                            self.dataArray = self.getDocument.selectedData?.resultData?.data.sorted(by: { $0.days_left < $1.days_left })
                             self.sortCopyArray = self.dataArray
                            
                          //  print(self.dataArray)
                        
                            
                            self.homeTableView.delegate = self
                            self.homeTableView.dataSource = self
                            self.homeTableView.reloadData()
                            self.homeTableView.isHidden = false
                            
                            
                            
                        }
                        else{
                            
                            if self.dataArray != nil{
                                
                                self.dataArray?.removeAll()
                                //self.homeTableView.reloadData()
                                
                                Alertift.alert(title: AppTitle, message: nodataFound)
                                    .action(.default("OK"))
                                    .show(on: self)
                            }
                            
                            
                        }
                        
                        
                        
                    }
                }
            }
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectedCategoryViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
            
            return (self.dataArray?.count)!
    
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    

        
        
        let infoData = dataArray![indexPath.row]
        
        // print(infoData)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditReminderVC") as? EditReminderViewController {
            // viewController.categoryId = self.catergoryID
            if let navigator = navigationController {
                
                if let companyId = GlobalDefault.value(forKey: "companyId"){
                    
                    cntCompnayId = companyId as? String
                    
                    
                    self.getDocumentForSelection(compnayID: cntCompnayId!, docId: self.categoryID!)
                    
                }
                
                guard let companyId = GlobalDefault.value(forKey: "companyId")  else {
                    
                    return
                }
                
                let cmpId = companyId as! String
                
                viewController.docId = infoData.id
                viewController.documentNumber = infoData.document_no
                viewController.dueDate = infoData.expiry_date
                viewController.descText = infoData.description
                viewController.companyId = cmpId
                viewController.docTypeId = infoData.document_type_id
                viewController.attachUrl = infoData.file_name
                navigator.pushViewController(viewController, animated: true)
                
                
                
                
                
            }
        }
        
        
        
       // self.dismiss(animated: false, completion: nil)
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

            
            
            
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath) as! ReminderCell
            
            let infoData = self.dataArray![indexPath.row]
            
        
        
            
        
            cell.lblTitle.text = infoData.description
            cell.lblDayCount.text = "\(infoData.days_left)"
        if infoData.days_left == 0 || infoData.days_left < 0{
           
            cell.circleView.backgroundColor = Utils.setType(type:"Expired")
            
        }
        else{
            
             cell.circleView.backgroundColor = Utils.setType(type:infoData.severity)
            
        }
        
        
            cell.lblExpiryDate.text = infoData.expiry_date
            cell.lblCategory.text = infoData.document_type
        
        
            return cell
        }
    
    
}
