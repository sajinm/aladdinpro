//
//  CategoryViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/1/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD

class CategoryViewController: BaseViewController {
    
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    @IBOutlet weak var categoryTitle: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dataArray:[categoryData]?
    var categoryTitleText:String?
    var categoryLoad = ResponseHandler()
    var selectedCategoryId:String?
    var selectedCategoryName:String?
    
    var isAddNewReminder:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        setInitialLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
       
       
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let companyId = GlobalDefault.value(forKey: "companyId"){
            
            let cmpId = companyId as? String
            loadCategories(companyId: cmpId!)
            
        }
        
        
    }
    
    
    func setInitialLoad() {
        
        categoryTableView.isHidden = true
        
        if (isAddNewReminder) {
            
            self.categoryTitle.text =  "Select a category"
            self.btnBack.isHidden = false
            
        }
        else{
            
            self.categoryTitle.text =  "Categories"
            self.btnBack.isHidden = true
            
            
            
        }
        
        
       
        
       
        
    }
    
    
    
    
    func loadCategories(companyId:String) {
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            SVProgressHUD.show()
            
            let params = [
                
              
                "company_id": companyId,
                "noOfRows":"100"
                
            
                
                ] as [String : Any]
            
            self.categoryLoad.getCategoryData(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess{
                    
                    
                    self.dataArray = self.categoryLoad.categoryData?.resultData?.data
                    
                    self.categoryTableView.isHidden = false
                    self.categoryTableView.delegate = self
                    self.categoryTableView.dataSource = self
                    self.categoryTableView.reloadData()
                    
                    
                }
                
            }
            
            
            
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    @IBAction func addNewCategory(_ sender: Any) {
        
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "catVC") as? AddNewCategoryViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
        
       // performSegue(withIdentifier: "toAddCategory", sender: Any?.self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toSelectedCategory" {
            
         
            let destinationVC = segue.destination as! SelectedCategoryViewController
            
            destinationVC.categoryID = self.selectedCategoryId
            destinationVC.categoryNameStr = self.selectedCategoryName
          
            
            
        }
    }


}

extension CategoryViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return (self.dataArray?.count ?? 0)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.categoryTableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath) as! ReminderCell
        
        let infoData = self.dataArray![indexPath.row]
        
        cell.lblTitle.text = infoData.name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if (isAddNewReminder){
            
            let infoData = self.dataArray![indexPath.row]
            
            //print(infoData)
            
            
            
            if #available(iOS 11.0, *) {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addNewVC") as? AddNewReminderViewController {
                    viewController.categoryId = infoData.id
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                // Fallback on earlier versions
            }
            
            
        }
        else{
            
             let infoData = self.dataArray![indexPath.row]
             self.selectedCategoryId = infoData.id
             self.selectedCategoryName = infoData.name
            
            self.performSegue(withIdentifier: "toSelectedCategory", sender: Any?.self)
            
            
        }
        

    }
    
    
    
    
    
}
