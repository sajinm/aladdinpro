//
//  PckageBillingViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/25/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD

class PckageBillingViewController: BaseViewController {
    
    @IBOutlet weak var btnSegmentControl: UISegmentedControl!
    
   
    @IBOutlet weak var subscriptionTableView: UITableView!{
    didSet {
    subscriptionTableView.tableFooterView = UIView(frame: .zero)
    //
    }
    }
    
    var getPackage = ResponseHandler()
    var packageString:String? = ""
    var memberShipId:String?
    var purposeString:String = ""
    
    var subscriptionData:[String]? = []
    var subcriptionHeader = ["Plan Name","Subscription Date","Renew Date"]
    
    var packages = [["title":"Small Business","price":"$204/Year"],["title":"Enterprise","price":"$408/Year"],["title":"Service Provider","price":"$648/Year"]]
    
    
    @IBOutlet weak var billingTableView: UITableView!{
        didSet {
            billingTableView.tableFooterView = UIView(frame: .zero)
        
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
         getBillingDetails()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func segmentPressed(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            
            self.billingTableView.isHidden = true
            self.subscriptionTableView.isHidden = false
            break
                      
           
        case 1:
          
            self.billingTableView.isHidden = false
            self.subscriptionTableView.isHidden = true
            break
            
            
        default:
          break
        }
        
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func initialSetup(){
        
        
        let font = UIFont.systemFont(ofSize: 12)
        btnSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        
         if  Connectivity.isConnectedToInternet {
            
            SVProgressHUD.show()
            
            getPackage.FetchPackages { (isSuccess, Message) in
                
              
                
                if isSuccess{
                    
            
                    
                    
                    //let data = ["a": 0, "b": 42]
                    let filtered = self.getPackage.packageData!.resultData.filter { $0.isCurrent == true} 
                    
                    if filtered.count > 0{
                        
                        self.packageString = filtered[0].name
                        
                       
                        
                    }
                    self.billingTableView.delegate = self
                    self.billingTableView.dataSource = self
                    self.billingTableView.reloadData()
                    
                    
                    
                    
                }
                else{
                    
                    self.billingTableView.delegate = self
                    self.billingTableView.dataSource = self
                    self.billingTableView.reloadData()
                    
                    
                }
                  SVProgressHUD.dismiss()
                
            }
            
         }else {
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
        
    }

    
    
    func getBillingDetails(){
        
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            SVProgressHUD.show()
            
            getPackage.FetchBilling { (isSuccess, Message) in
                
                
                
                if isSuccess{
                    
                    if self.getPackage.billingData?.resultCode == 1{
                        
                        if let name = self.getPackage.billingData?.resultData?.subscription?.name{
                           
                            self.subscriptionData?.append(name)
                            
                        }
                        
                        if let subDate = self.getPackage.billingData?.resultData?.subscription?.subscription_date{
                            
                            self.subscriptionData?.append(subDate)
                            
                        }
                        
                        if let renewDate = self.getPackage.billingData?.resultData?.subscription?.renew_date{
                            
                            self.subscriptionData?.append(renewDate)
                            
                        }
                        
                        
                        self.subscriptionTableView.delegate = self
                        self.subscriptionTableView.dataSource = self
                        self.subscriptionTableView.reloadData()
                        
                        
                    }
                    
                    
                    
                    
                   
                    
                    
                    
                }
                else{
                    
                   
                    
                    
                }
                SVProgressHUD.dismiss()
                
            }
            
        }else {
            
            Alertift.alert(title: AppTitle, message: noInternrt)
                .action(.default("OK"))
                .show(on: self)
            
        }
        
        
        
    }

    

}








extension PckageBillingViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let cell = self.billingTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
        
        cell.btnUpgrade.isHidden = true
        
        if tableView == self.billingTableView {
            
          
        
        cell.lblPrice.text = packages[indexPath.row]["price"]
        cell.menuTitle.text = packages[indexPath.row]["title"]
 
        
       
            
            if packageString == "Small Business" {
                
            
                   memberShipId = "3"
                
                if indexPath.row == 0{
                    
                    cell.btnUpgrade.setTitle("Renew", for: .normal)
                    cell.menuTitle.textColor = UIColor.orange
                    purposeString = "renew"
                    memberShipId = "3"
                    
                }
                if indexPath.row == 1{
                    
                    cell.btnUpgrade.setTitle("Upgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "upgrade"
                    memberShipId = "4"
                    
                }
                if indexPath.row == 2{
                    
                    cell.btnUpgrade.setTitle("Upgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "upgrade"
                    memberShipId = "5"
                    
                }
            
                
            }else if(packageString == "Enterprise"){
                
            
                
                if indexPath.row == 0{
                    
                    cell.btnUpgrade.setTitle("Downgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                     purposeString = "upgrade"
                     memberShipId = "3"
                    
                }
                if indexPath.row == 1{
                    
                    cell.btnUpgrade.setTitle("Renew", for: .normal)
                    cell.menuTitle.textColor = UIColor.orange
                    purposeString = "renew"
                    memberShipId = "4"
                    
                }
                if indexPath.row == 2{
                    
                    cell.btnUpgrade.setTitle("Upgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "upgrade"
                    memberShipId = "5"
                }
                
                
                
                
            }else if (packageString == "Service Provider"){
                
               
                
                if indexPath.row == 0{
                    
                    cell.btnUpgrade.setTitle("Downgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                     purposeString = "upgrade"
                     memberShipId = "3"
                    
                }
                if indexPath.row == 1{
                    
                    cell.btnUpgrade.setTitle("Downgrade", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                     purposeString = "upgrade"
                     memberShipId = "4"
                }
                if indexPath.row == 2{
                    
                    cell.btnUpgrade.setTitle("Renew", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "renew"
                     memberShipId = "5"
                    
                }
                
                
            }else{
                
                
                if indexPath.row == 0{
                    
                    cell.btnUpgrade.setTitle("Buy", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "registration"
                    memberShipId = "3"
                    
                }
                if indexPath.row == 1{
                    
                    cell.btnUpgrade.setTitle("Buy", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "registration"
                    memberShipId = "4"
                }
                if indexPath.row == 2{
                    
                    cell.btnUpgrade.setTitle("Buy", for: .normal)
                    cell.menuTitle.textColor = UIColor.darkGray
                    purposeString = "registration"
                    memberShipId = "5"
                    
                }
                
                
                
                
                
                
        }
        
        }else{
            
            if (cell.btnUpgrade != nil){
                
                cell.btnUpgrade.isHidden = true
                
            }
            
            cell.lblPrice.text = subscriptionData![indexPath.row]
            cell.menuTitle.text = subcriptionHeader[indexPath.row]
            
            
            
            
        }
       
        
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//                    guard let userId = GlobalDefault.value(forKey: "userID") else{
//
//                        return
//                    }
//
//        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as? PaymentViewController {
//            if let navigator = navigationController {
//
//                viewController.userId = userId as? String
//                viewController.purposeStr = purposeString
//                viewController.memberShipId = memberShipId
//                navigator.pushViewController(viewController, animated: true)
//            }
//        }
//
  }
    
    
}
