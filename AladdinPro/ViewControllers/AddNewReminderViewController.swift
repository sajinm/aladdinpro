//
//  AddNewReminderViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/3/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alertift
import SVProgressHUD
import VisionKit
import PDFKit
import Toast_Swift



class AddNewReminderViewController: BaseViewController {
    
    var categoryId:String?
    var docTypeId:String?
    
    
    @IBOutlet weak var txtReferenceNumber: HoshiTextField!
    @IBOutlet weak var txtDescription: HoshiTextField!
    @IBOutlet weak var txtDueDate: HoshiTextField!
    
    @IBOutlet weak var btnRemove: UIButton!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var btnPDFView: UIButton!
    
    var finalImageArray:[UIImage] = []
    var finalIDArray:[UIImage] = []

    @IBOutlet weak var docTypeSegment: UISegmentedControl!
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var thumbCollectionView: UICollectionView!
    var newReminder = ResponseHandler()
    
    var datePicker = UIDatePicker()
    
    var dateStr:String? = ""
    var isScan:Bool = false
    var isDocument:Bool = true
    
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnId: UIButton!
    
    @IBOutlet weak var attachmentView: UIView!
    @IBOutlet weak var attachmentSelectedMainImage: UIImageView!
    
    @IBOutlet weak var idCardView: UIView!
    @IBOutlet weak var idFirstImageView: UIImageView!
    @IBOutlet weak var idSecondImageView: UIImageView!
    
    @IBOutlet weak var lblFrontSide: UILabel!
    @IBOutlet weak var lblBackSide: UILabel!
    @IBOutlet weak var thumbNailView: UIImageView!
    @IBOutlet weak var btnUpoload: UIButton!
    
    @available(iOS 11.0, *)
    lazy var pdfDocument = PDFDocument()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnPDFView.isHidden = true
        idCardView.isHidden = true
        idSecondImageView.isHidden = true
        self.lblBackSide.isHidden = true
        self.lblFrontSide.isHidden = true
        attachmentView.isHidden = true
      
        self.btnRemove.isHidden = true
        self.imagePicker.delegate = self
        datePicker.datePickerMode = .date
        txtDueDate.inputView = datePicker
        self.datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        if #available(iOS 13.0, *){
            
            self.docTypeSegment.backgroundColor = UIColor.white
            
            
        }else{
            
             self.docTypeSegment.backgroundColor = UIColor.black
        }
    
        

       // print(self.categoryId!)
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        
        let newDate:String = ""
        
        self.txtDueDate.text = newDate.formatDate(date: datePicker.date)
        
       // self.dateStr = newDate.formatDate(date: datePicker.date)
    }
    
    
    @IBAction func docSelection(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.btnDoc.setTitleColor(.white, for: .normal)
             self.btnId.setTitleColor(.lightGray, for: .normal)
             self.isDocument = true
             self.idCardView.isHidden = true
             self.thumbCollectionView.reloadData()
        case 1:
            
             self.thumbCollectionView.reloadData()
        
            if finalImageArray.count > 0 {
           
            if finalImageArray.count == 1{
                
                self.idFirstImageView.image = finalImageArray[0]
                self.idSecondImageView.isHidden = false
                self.lblBackSide.isHidden = false
            }
            
            if finalImageArray.count >= 2 {
                
                if finalImageArray.count > 2{
                    
                    for _ in 0..<1 {
                        
                        self.finalImageArray.removeLast()
                        
                    }
                    
                    self.thumbCollectionView.reloadData()
                    
                }
                
                self.idFirstImageView.image = finalImageArray[0]
                self.idSecondImageView.image = finalImageArray[1]
                self.idSecondImageView.isHidden = false
                
            }
            }else{
                
                self.lblFrontSide.isHidden = false
                
                
            }
            
            
            
            self.btnDoc.setTitleColor(.lightGray, for: .normal)
            self.btnId.setTitleColor(.white, for: .normal)
            self.isDocument = false
            self.idCardView.isHidden = false
            
        default:
            break
        }
        
        
        
    }
    
    
    func submitReminder(){
        
        if  Connectivity.isConnectedToInternet {
            
            if (txtReferenceNumber.text?.isEmpty)! ||  (txtDescription.text?.isEmpty)! ||  (txtDueDate.text?.isEmpty)! {
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }else{
                
                guard let categoryId = self.categoryId else {
                    
                    return
                }
                
                guard let companyId = GlobalDefault.value(forKey: "companyId") else{
                    
                    return
                }
                
               // self.setToast(message:"message",view:self.view,onController:self.navigationController!)
                SVProgressHUD.show()
                
                let params = [
                    
                    
                    "docTypeSelection": categoryId,
                    "company_id":companyId,
                    "expiry_date":txtDueDate.text!,
                    "document_no":txtReferenceNumber.text!,
                    "description":txtDescription.text!,
                    "is_notifiable":"true"
                    
                    
                    
                    ] as [String : Any]
                
                if #available(iOS 11.0, *) {
                    
                    self.newReminder.addNewReminderWithImage(withParameter: params, imageArray: pdfDocument) { (isSuccess, message) in
                        
                       SVProgressHUD.dismiss()
                        
                        if isSuccess{
                            
                            DispatchQueue.main.async {

                                    self.setToast(message:message,view:self.view,onController:self.navigationController!)
  
      
                            }
                            
                            
                            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as? HomeViewController {
                                if let navigator = self.navigationController {
                                    navigator.pushViewController(viewController, animated: true)
                                }
                            }
                            //self.navigationController?.popViewController(animated: true)
                            
                            
                        }
                        
                    }
                } else {
                    // Fallback on earlier versions
                }
            
                
            }
        
        }else{
            
            //self.setToast(message:noInternrt,onController:self.navigationController!)
            
//            Alertift.alert(title: AppTitle, message: noInternrt)
//                .action(.default("OK"))
//                .show(on: self)
//
            
            
        }
        
    }
    
    
    @IBAction func viewPdfView(_ sender: Any) {
        
        if #available(iOS 11.0, *) {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PdfVC") as? PDFViewViewController {
                viewController.pdfDocument = self.pdfDocument
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    
    
    
    
    @available(iOS 11.0, *)
    @IBAction func removeImagePressed(_ sender: Any) {
        
        
        self.btnRemove.isHidden = true
        self.btnUpdate.isHidden = false
        self.btnPDFView.isHidden = true
        
        if self.pdfDocument.pageCount > 0{
            
            for i in 0..<self.pdfDocument.pageCount{
                      
                       pdfDocument.removePage(at: i)
                       
                   }
            self.thumbNailView.image = nil
            
        }
        
       
        
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        submitReminder()
        
    }
    
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
           
           switch sender.selectedSegmentIndex {
           case 0:
            
              self.isScan = false
              getLibrary()
            
           case 1:
               
            self.isScan = true
            goForscan()
               
           default:
               break
           }
           
           
       }
    
    
    func goForscan(){
        
        
       
       
                   if #available(iOS 13.0, *) {
                       let scannerViewController = VNDocumentCameraViewController()
                       scannerViewController.delegate = self
                       present(scannerViewController, animated: true)

                   } else {


                    
                    imagePicker.allowsEditing = true
                    imagePicker.sourceType = .camera
                    present(imagePicker, animated: true, completion: nil)
                    
                      
                }
                    
        
        
    }
    
    func getLibrary(){
        
        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.thumbCollectionView.reloadData()
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    func resetAllImageView(){
        
        
        self.docTypeSegment.selectedSegmentIndex = 0
        self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
        self.idFirstImageView.image = nil
        self.idSecondImageView.image = UIImage(named: "backCard")
        self.idSecondImageView.isHidden = true
        
        self.btnDoc.setTitleColor(.white, for: .normal)
         self.btnId.setTitleColor(.lightGray, for: .normal)
         self.isDocument = true
         self.idCardView.isHidden = true
        self.finalImageArray.removeAll()
        self.thumbCollectionView.reloadData()
        
        
    }
    
    
    @IBAction func uploadPressed(_ sender: Any) {
        
        
       
        
        attachmentView.isHidden = false
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    
     @IBAction func viewPdf(_ sender: Any){
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "catVC") as? AddNewCategoryViewController {
                   if let navigator = navigationController {
                       navigator.pushViewController(viewController, animated: true)
                   }
               }
        
        
    }
    
    
    @IBAction func donePressed(_ sender: Any) {
        
       
        if finalImageArray.count > 0 {
            
        
        
        if #available(iOS 11.0, *) {
            
             pdfDocument = PDFDocument()
            
            
            if !isDocument {
                
                
                var size = 0.5
                                               
                if GlobalDefault.value(forKey: "size") != nil{

                size = GlobalDefault.value(forKey: "size") as! Double

                }
                                
                
                if self.finalImageArray.count > 1{
                    
                    
                    let mergedPic = self.finalImageArray[0].mergeWith(topImage: self.finalImageArray[1])
                    guard let compressedImage = mergedPic.jpegData(compressionQuality: CGFloat(size)) else { return }
                    
                     let pdfPage = PDFPage(image: UIImage(data: compressedImage)!)
                    

                        pdfDocument.insert(pdfPage!, at: 0)
                        
                
                    
                }else{
                    
                    
                     let compressImage = self.finalImageArray[0].jpegData(compressionQuality: CGFloat(size))!
                    
                     let pdfPage = PDFPage(image: UIImage(data: compressImage)!)
                                        
                                         pdfDocument.insert(pdfPage!, at: 0)
                    
                }
                
                
            }else{
                
                for i in 0 ..< self.finalImageArray.count {
                               
                               var size = 0.5
                               
                               if GlobalDefault.value(forKey: "size") != nil{
                                   
                                   size = GlobalDefault.value(forKey: "size") as! Double
                                   
                               }
                               
                               if let image = self.finalImageArray[i].jpegData(compressionQuality: CGFloat(size))    {
                              
                             
                                   let pdfPage = PDFPage(image: UIImage(data: image)!)
                              
                               // Insert the PDF page into your document
                               pdfDocument.insert(pdfPage!, at: i)
                           }
                       }
                
                
            }
            
            
        
        
           
            
            
         
        
            let image = makeThumbnail(pdfDocument: pdfDocument, page: 0)
       
            self.thumbNailView.image = image
            self.thumbNailView.contentMode = .scaleAspectFit
            
            self.attachmentView.isHidden = true
            self.btnRemove.isHidden = false
            
            self.btnUpdate.isHidden = true
            self.btnPDFView.isHidden = false
            
          }
         resetAllImageView()
        
      }
        
    }
    
    
    @available(iOS 11.0, *)
    func makeThumbnail(pdfDocument: PDFDocument?, page: Int) -> UIImage? {
        return pdfDocument?.page(at: page)?.thumbnail(of: CGSize(width: self.thumbNailView.frame.width, height: self.thumbNailView.frame.height), for: .artBox)
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        resetAllImageView()
        
        self.attachmentView.isHidden = true
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddNewReminderViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
           
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
           
            picker.dismiss(animated: true, completion: nil)
        }
        
        
        
        if GlobalDefault.bool(forKey: "isMonochrome") != nil{
            
            if GlobalDefault.bool(forKey: "isMonochrome"){
                
                
                let bwImage = selectedImage?.blackAndWhite()
                self.attachmentSelectedMainImage.image = bwImage
                self.finalImageArray.append(bwImage!)
                
                
            }else{
                
                self.attachmentSelectedMainImage.image = selectedImage
                self.finalImageArray.append(selectedImage!)
                
                
            }
            
            
        }else{
            
            self.attachmentSelectedMainImage.image = selectedImage
            self.finalImageArray.append(selectedImage!)
            
        }
        
       
        
        if self.finalImageArray.count > 0{
            
            if !isDocument{
                
                
                
                if self.finalImageArray.count > 1{
                    self.idFirstImageView.image = self.finalImageArray[0]
                    self.lblFrontSide.isHidden = true
                    self.idSecondImageView.image = self.finalImageArray[1]
                    self.lblBackSide.isHidden = true
                }else{
                    
                    if self.finalImageArray.count == 1{
                        self.idFirstImageView.image = self.finalImageArray[0]
                        self.lblFrontSide.isHidden = true
                        self.idSecondImageView.isHidden = false
                         self.lblBackSide.isHidden = false
                        
                    }
                    
             
                    
                }
                
            }
            
            
            self.thumbCollectionView.delegate = self
            self.thumbCollectionView.dataSource = self
            self.thumbCollectionView.reloadData()
            
        }
        //self.btnUpdate.setTitle("Attachment", for: .normal)
        self.btnUpdate.isHidden = false
       
        //self.btnRemove.isHidden = false
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
                  self.thumbCollectionView.delegate = self
                  self.thumbCollectionView.dataSource = self
                  self.thumbCollectionView.reloadData()
               
               dismiss(animated: true, completion: nil)
    }
    
    
   
    
}
@available(iOS 13.0, *)
extension AddNewReminderViewController:VNDocumentCameraViewControllerDelegate{
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        // Process the scanned pages
        
        var scannedImage:UIImage?
        
        
        for pageNumber in 0..<scan.pageCount {
            let image = scan.imageOfPage(at: pageNumber)
            scannedImage = image
       
        if let image = scannedImage{
            
            if GlobalDefault.bool(forKey: "isMonochrome") != nil{
                
                
                if GlobalDefault.bool(forKey: "isMonochrome"){
                    
                    self.finalImageArray.append(image.blackAndWhite()!)
                    scannedImage = image.blackAndWhite()!
                    
                }else{
                    
                    self.finalImageArray.append(image)
                }
                
                
            }else{
               
                   self.finalImageArray.append(image)
                
            }
        
          
        }
        
        
        
        
        
        
        if !isDocument{
            
            
            self.idCardView.isHidden = false
        
            if self.finalImageArray.count > 0{
                
                
              
                        if self.finalImageArray.count == 1{
                            
                            self.idFirstImageView.image = self.finalImageArray[0]
                            self.lblFrontSide.isHidden = true
                            self.idSecondImageView.isHidden = false
                            self.lblBackSide.isHidden = false
                            
                            
                        }
                        
                        if self.finalImageArray.count > 1{
                            
                            self.idFirstImageView.image = self.finalImageArray[0]
                            self.lblFrontSide.isHidden = true
                            self.idSecondImageView.image = self.finalImageArray[1]
                            self.lblBackSide.isHidden = true
                            
                            
                        }
                        

            }
            
            
        }
        
        else{
            
   
            
            self.attachmentSelectedMainImage.image = scannedImage!
            
             self.idCardView.isHidden = true
            
        }
        
        }
               

        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.thumbCollectionView.reloadData()
        // You are responsible for dismissing the controller.
        controller.dismiss(animated: true)
    }
    
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        // You are responsible for dismissing the controller.
        
        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.thumbCollectionView.reloadData()
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        // You should handle errors appropriately in your app.
    
        // You are responsible for dismissing the controller.
        controller.dismiss(animated: true)
    }
    
    
}

extension AddNewReminderViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !isDocument{
            
            if self.finalImageArray.count >= 2{
                
                return 2
            }else{
                
                 return self.finalImageArray.count + 1
                
            }
            
          
            
        }else{
            
            return self.finalImageArray.count + 1
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = thumbCollectionView.dequeueReusableCell(withReuseIdentifier: "thumbnailCell", for: indexPath) as! ThumbViewCell
        
        
       
      
    
        if indexPath.row < self.finalImageArray.count{
        
            
            cell.thumbPic.image = self.finalImageArray[indexPath.row]
            
            if cell.thumbPic.image != nil{
                
                cell.btnRemove.isHidden = false
                cell.indexValue = indexPath.row
                cell.delegate = self
               
                    
                    
            }else{
                
                
                 cell.btnRemove.isHidden = true
                
                
            }
            
            }else{
            
          //  if isDocument{
            
            cell.btnRemove.isHidden = true
            cell.thumbPic.image = UIImage(named: "ic_newScan")
      //      }
            
//            else if isScan && self.finalImageArray.count == 2{
//
//                cell.btnRemove.isHidden = true
//                cell.thumbPic.isHidden = true
//
//            }
        }
            
        
        
    
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == self.finalImageArray.count{
            
            if self.isScan{
                
              goForscan()
                
            }else{
                
               
              getLibrary()
                
            }
            
            
            
        }else{
            
            
            
            self.attachmentSelectedMainImage.image = self.finalImageArray[indexPath.row]
            
        }
        
        
    }
    
    
    
    
}

extension AddNewReminderViewController:ThumbDeleteProtocol{
    
    func removeThumbnail(at index: Int) {
        

        self.finalImageArray.remove(at: index)
        self.thumbCollectionView.reloadData()
        
     
        
        if !isDocument{
            
                        
            
        if finalImageArray.count == 1{
                       
                       self.idFirstImageView.image = finalImageArray[0]
                       self.idSecondImageView.isHidden = false
                       self.idSecondImageView.image = UIImage(named: "backCard")
                       self.lblBackSide.isHidden = false
                   }
                   
                   if finalImageArray.count >= 2 {
                       
                       self.idFirstImageView.image = finalImageArray[0]
                       self.idSecondImageView.image = finalImageArray[1]
                       self.idSecondImageView.isHidden = false
                       
                   }
            
            if finalImageArray.count == 0{
                
                self.idFirstImageView.image = UIImage(named: "backCard")
                self.idSecondImageView.image = UIImage(named: "backCard")
                self.idSecondImageView.isHidden = true
                self.lblBackSide.isHidden = true
                self.lblFrontSide.isHidden = false
                
            
        }
    
    }else{
            if finalImageArray.count >= 1{
                
              
                if index > 0{
                   
                    self.attachmentSelectedMainImage.image = finalImageArray[index - 1]
                    
                }else{
                    
                     self.attachmentSelectedMainImage.image = finalImageArray[index]
                     //self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
                }
               
                 
                
            }else{
                
                self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
                
            }
           
    
    
    }
    
    }
    
}
