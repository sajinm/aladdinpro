//
//  ForgotPasswordViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 7/26/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: ImageTextField!
    
   
    let ApiHandler = ResponseHandler()
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        txtEmail.resignFirstResponder()
        if  Connectivity.isConnectedToInternet {
            
            if (txtEmail.text?.isEmpty)! || !(txtEmail.text?.isValidEmail())!   {
                
                Alertift.alert(title: AppTitle, message:validEmail)
                    .action(.default("OK"))
                    .show(on: self)
                
                
            }else {
                
                let params = [
                    "email": txtEmail.text!
                    
                    
                    ] as [String : Any]
                
                SVProgressHUD.show()
                
                ApiHandler.checkPassword(withParameter: params) { (isSuccess, message) in
                   SVProgressHUD.dismiss()
                    self.txtEmail.text = ""
                    if isSuccess{
                        
                        Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                        
                    }
                    else{
                        
                        return
                        
                    }
                   
                    
                }
                
                
                
            }
            
        }
        
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
