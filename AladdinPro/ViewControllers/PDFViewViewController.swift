//
//  PDFViewViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 16/04/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
class PDFViewViewController: UIViewController {
    var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
    
    @IBOutlet weak var pdfView: PDFView!
    
    var pdfDocument:PDFDocument?
    var pdfUrl:String?
    var imageFile:UIImage?
    var fileType:String?

    @IBOutlet weak var showImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let pdfDocument = pdfDocument {
            showImageView.isHidden = true
            self.pdfView.isHidden = false
            pdfView.displayMode = .singlePageContinuous
            pdfView.autoScales = true
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
            
            showImageView.isHidden = true
        }
        
        if let urlStr = pdfUrl {
             self.pdfView.isHidden = false
             showImageView.isHidden = true
             self.pdfView.isHidden = false
            if let pdfDocument = PDFDocument(url: URL(string: urlStr)!) {
                pdfView.displayMode = .singlePageContinuous
                pdfView.autoScales = true
                pdfView.displayDirection = .vertical
                pdfView.document = pdfDocument
            }
            
            
            
        }
        
        if let image = self.imageFile {
            
            self.pdfView.isHidden = true
            showImageView.image = image
            showImageView.isHidden = false
            
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func whatsappShareWithImages(_ sender: AnyObject) {
        
        if self.imageFile != nil{
            
       let urlWhats = "whatsapp://app"
              if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed) {
                  if let whatsappURL = URL(string: urlString) {
                      
                      if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                          
                          if let image = self.imageFile {
                              if let imageData = image.jpegData(compressionQuality: 1.0) {
                                  let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/AladdinPro")
                                  do {
                                      try imageData.write(to: tempFile, options: .atomic)
                                      self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
                                      self.documentInteractionController.uti = "net.whatsapp.image"
                                      self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                      
                                  } catch {
                                      print(error)
                                  }
                              }
                          }

                      } else {
                          // Cannot open whatsapp
                      }
                  }
              }
        }else{
            
            
            let urlWhats = "whatsapp://app"
                   if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed) {
                       if let whatsappURL = URL(string: urlString) {
                           
                           if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                               
                               if let image = self.pdfDocument {
                                
                                
                               
                                  
                                let documentsPath = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/AladdinPro.pdf")//(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL

                                   
                        
                                       do {
                                        
                                        let documentsURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                                        let pdfDocURL = documentsURL.appendingPathComponent("document.pdf")
                                        let pdfData = image.dataRepresentation()
                                        try pdfData!.write(to: pdfDocURL)
                                        
                                        
                                        let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                                        let fileDocURL = docURL.appendingPathComponent("document.pdf")
                                        
                                        let document = NSData(contentsOf: fileDocURL)
                                        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [document!], applicationActivities: nil)
                                        activityViewController.popoverPresentationController?.sourceView=self.view
                                        present(activityViewController, animated: true, completion: nil)
                                        
                                        
                                        
//                                        let fileManager = FileManager.default
//                                        let documentoPath = "abc.pdf"
//                                        if fileManager.fileExists(atPath: documentoPath!){
//                                          let url = URL(fileURLWithPath: documentoPath)
//                                          let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                                        
                                        
//                                          activityViewController.popoverPresentationController?.sourceView=self.view
//                                          self.present(activityViewController, animated: true, completion: nil)
//                                        }else {
//                                           displayAlertWithMessage("Something Went Wrong")
//                                        }
                    
                                        
//                                        let data = try Data(contentsOf: documentsPath)
//                                        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
//                                        activityViewController.popoverPresentationController?.sourceView = self.view
//
//                                        self.present(activityViewController, animated: true, completion: nil)
//                                        print("pdf file loading...")
                                        
                                        
                                        
                                        
//                                           image.write(to: documentsPath)
//                                           self.documentInteractionController = UIDocumentInteractionController(url: documentsPath)
//                                           self.documentInteractionController.uti = "net.whatsapp.pdf"
//                                           self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                           
                                       } catch {
                                           print(error)
                                       }
                                   
                               }

                           } else {
                               // Cannot open whatsapp
                           }
                       }
                   }

            
            
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

