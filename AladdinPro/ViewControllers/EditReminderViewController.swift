//
//  EditReminderViewController.swift
//  AladdinPro
//
//  Created by Sajin M on 4/6/19.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
import Alertift
import SDWebImage
import PDFKit
import VisionKit


class EditReminderViewController: BaseViewController {
    
    @IBOutlet weak var txtReferenceNumber: HoshiTextField!
    
    @IBOutlet weak var txtDescription: HoshiTextField!
    
    @IBOutlet weak var pdfActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txtDueDate: HoshiTextField!
    @IBOutlet weak var btnRemove: UIButton!
    
    @IBOutlet weak var btnFullView: UIButton!
    @IBOutlet weak var imagePickButton: UIButton!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var btnImageView: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var attachmentImage: UIImageView!
    
    @IBOutlet weak var attachmentSelectedMainImage: UIImageView!
       
    @IBOutlet weak var idCardView: UIView!
    @IBOutlet weak var idFirstImageView: UIImageView!
    @IBOutlet weak var idSecondImageView: UIImageView!
    
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnId: UIButton!
    @IBOutlet weak var lblBackSide: UILabel!
    
    @IBOutlet weak var attachmentView: UIView!
    
    @IBOutlet weak var lblFrontSide: UILabel!
    
    @IBOutlet weak var thumbCollectionView: UICollectionView!
    
    var finalImageArray:[UIImage] = []
    
    var isScan:Bool = false
    var isDocument:Bool = true
    
    @IBOutlet weak var docTypeSegment: UISegmentedControl!

    
   // @IBOutlet weak var imageFullView: UIView!
    let imagePicker = UIImagePickerController()
    
    
    var isAttachmentEdited:Bool?
    var docId:String?
    var docTypeId:String?
    var descText:String?
    var dueDate:String?
    var attachUrl:String?
    var refrenceNumber:String?
    var companyId:String?
    var documentNumber:String?
    var datePicker = UIDatePicker()
    @available(iOS 11.0, *)
    lazy var pdfDocument = PDFDocument()
    
    

    
    
    @IBOutlet weak var fullImage: UIImageView!
    var deleteReminder = ResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if #available(iOS 13.0, *){
                   
                   self.docTypeSegment.backgroundColor = UIColor.white
                   
                   
               }else{
                   
                    self.docTypeSegment.backgroundColor = UIColor.black
               }
           
        
        SVProgressHUD.dismiss()
        self.pdfActivityIndicator.isHidden = true
        btnRemove.isHidden = true
    //imageFullView.isHidden = true
        attachmentImage.isHidden = true
        btnFullView.isHidden = true
        idCardView.isHidden = true
        idSecondImageView.isHidden = true
        self.lblBackSide.isHidden = true
              
         attachmentView.isHidden = true
       
       // imageScrollView.isHidden = true
        
   
        self.imagePicker.delegate = self
        imagePicker.mediaTypes = ["public.image"]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let newDate = dateFormatter.date(from: dueDate!)
        
        txtReferenceNumber.text = documentNumber
        txtDueDate.text = dueDate?.formatDate(date: newDate!)
        txtDescription.text = descText
        
        if let fileTxt = attachUrl  {
             if fileTxt != "" {
               attachmentImage.isHidden = false
                btnImageView.isHidden = false
                imagePickButton.isHidden = true
                btnFullView.isHidden = false
           
                self.isAttachmentEdited = false
                if fileTxt.fileExtension() == "pdf"{
                    
                  //  print(imageUrl+fileTxt)
                    
                
                    self.pdfActivityIndicator.isHidden = false
                   // attachmentImage.sd_setImage(with: URL(string: imageUrl + fileTxt), placeholderImage: UIImage(named: "ic_pdf"))
                   attachmentImage.sd_setShowActivityIndicatorView(true)
                    attachmentImage.sd_setIndicatorStyle(.gray)
                    fetchPdfFile(url: imageUrl + fileTxt)

                }else if (fileTxt.fileExtension() == "doc" || fileTxt.fileExtension() == "docx"){
                    
                    attachmentImage.sd_setImage(with: URL(string: imageUrl + fileTxt), placeholderImage: UIImage(named: "ic_word"))
                    
                    
                    
                }
                else{
                    
                    self.btnRemove.isHidden = false
                    attachmentImage.sd_setShowActivityIndicatorView(true)
                    attachmentImage.sd_setIndicatorStyle(.gray)
                    attachmentImage.sd_setImage(with: URL(string: imageUrl + fileTxt), placeholderImage: UIImage(named: "placeholder.png"))
                    
//                     ge.sd_setImage(with: URL(string: imageUrl + fileTxt), placeholderImage: UIImage(named: "placeholder.png"))
                   
                    
                }
                
           
            }
        }
        

        txtDueDate.inputView = datePicker
        datePicker.datePickerMode = .date
        self.datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func showFullImage(_ sender: Any) {
        
        
        if self.attachUrl?.fileExtension() == "pdf"{
            
            
            if #available(iOS 11.0, *) {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PdfVC") as? PDFViewViewController {
                    viewController.pdfDocument = self.pdfDocument
                    viewController.fileType = "pdf"
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                // Fallback on earlier versions
            }

            
            
        } else if self.attachUrl?.fileExtension() == "doc" || self.attachUrl?.fileExtension() == "docx" {
            
            if #available(iOS 11.0, *) {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PdfVC") as? PDFViewViewController {
                   // viewController.imageFile = self.attachmentImage.image
                    viewController.fileType = self.attachUrl?.fileExtension()
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                // Fallback on earlier versions
            }
            
            
            
        }
        
        else {
            
            if #available(iOS 11.0, *) {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PdfVC") as? PDFViewViewController {
                    viewController.imageFile = self.attachmentImage.image
                    viewController.fileType = self.attachUrl?.fileExtension()
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                // Fallback on earlier versions
            }
            
            // self.imageFullView.isHidden = false
            
        }
        
       
        
        
    }
    
    
    
    func fetchPdfFile(url:String){
        
        guard let url = URL(string: url) else { return }
        
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
    
    }
    
    
    @IBAction func docSelection(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.btnDoc.setTitleColor(.white, for: .normal)
             self.btnId.setTitleColor(.lightGray, for: .normal)
            self.isDocument = true
             self.idCardView.isHidden = true
        case 1:
           
           
          
                 
                     if finalImageArray.count > 0 {
                    
                     if finalImageArray.count == 1{
                         
                         self.idFirstImageView.image = finalImageArray[0]
                         self.idSecondImageView.isHidden = false
                         self.lblBackSide.isHidden = false
                     }
                     
                     if finalImageArray.count >= 2 {
                         
                         if finalImageArray.count > 2{
                             
                             for _ in 0..<1 {
                                 
                                 self.finalImageArray.removeLast()
                                 
                             }
                             
                            
                             
                         }
                         
                         
                         self.thumbCollectionView.reloadData()
                         self.idFirstImageView.image = finalImageArray[0]
                         self.idSecondImageView.image = finalImageArray[1]
                         self.idSecondImageView.isHidden = false
                         
                     }
                     }else{
                         
                         self.lblFrontSide.isHidden = false
                         
                         
                     }
                     
            
            
            self.btnDoc.setTitleColor(.lightGray, for: .normal)
            self.btnId.setTitleColor(.white, for: .normal)
            self.isDocument = false
            self.idCardView.isHidden = false
            
        default:
            break
        }
        
        
        
    }
    
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
         
           self.isScan = false
           getLibrary()
         
        case 1:
            
         self.isScan = true
         goForscan()
            
        default:
            break
        }
        
        
    }
    
    func goForscan(){
           
        
                      if #available(iOS 13.0, *) {
                          let scannerViewController = VNDocumentCameraViewController()
                          scannerViewController.delegate = self
                          present(scannerViewController, animated: true)
                          
                      } else {
                        
                                          imagePicker.allowsEditing = true
                                          imagePicker.sourceType = .camera
                                          present(imagePicker, animated: true, completion: nil)
                        
                          // Fallback on earlier versions
                      }
                       
           
           
       }
    
    
    func getLibrary(){
           
           self.thumbCollectionView.delegate = self
           self.thumbCollectionView.dataSource = self
           self.thumbCollectionView.reloadData()
           
           imagePicker.allowsEditing = false
           imagePicker.sourceType = .photoLibrary
           
           present(imagePicker, animated: true, completion: nil)
           
       }
    
    
    
    @IBAction func donePressed(_ sender: Any) {
         
        
         if finalImageArray.count > 0 {
             
         
         
         if #available(iOS 11.0, *) {
             
              pdfDocument = PDFDocument()
         
         
                       if !isDocument {
                     
                     
                     var size = 0.75
                                                    
                     if GlobalDefault.value(forKey: "size") != nil{
                                                        
                     size = GlobalDefault.value(forKey: "size") as! Double
                                                        
                     }
                                     
                     
                     if self.finalImageArray.count > 1{
                        
                        
                           let mergedPic = self.finalImageArray[0].mergeWith(topImage: self.finalImageArray[1])
                                               guard let compressedImage = mergedPic.jpegData(compressionQuality: CGFloat(size)) else { return }
                                               
                                                let pdfPage = PDFPage(image: UIImage(data: compressedImage)!)
                                               

                                                   pdfDocument.insert(pdfPage!, at: 0)
                                                   
                                        
                         
                     }else{
                         
                         
                          let compressImage = self.finalImageArray[0].jpegData(compressionQuality: CGFloat(size))!
                         
                          let pdfPage = PDFPage(image: UIImage(data: compressImage)!)
                                             
                                              pdfDocument.insert(pdfPage!, at: 0)
                         
                     }
                     
                     
                       }else{
                        
                        
                        

                            for i in 0 ..< self.finalImageArray.count {
                                
                                var size = 0.5
                                
                                if GlobalDefault.value(forKey: "size") != nil{
                                    
                                    size = GlobalDefault.value(forKey: "size") as! Double
                                    
                                }
                                
                                if let image = self.finalImageArray[i].jpegData(compressionQuality: CGFloat(size))    {
                               
                              
                                    let pdfPage = PDFPage(image: UIImage(data: image)!)
                               
                                // Insert the PDF page into your document
                                pdfDocument.insert(pdfPage!, at: i)
                            }
                        }
                        
                        
                        
            }
                 
                 
        
         
             let image = makeThumbnail(pdfDocument: pdfDocument, page: 0)
             self.attachmentImage.isHidden = false
             self.attachmentImage.image = image
             self.attachmentImage.contentMode = .scaleAspectFit
             imagePickButton.isHidden = true
             self.attachmentView.isHidden = true
             self.btnRemove.isHidden = false
             btnFullView.isHidden = false
          
             
           }
            
        self.isAttachmentEdited = true
          resetAllImageView()
         
       }
         
     }
    
    func resetAllImageView(){
           
           
           self.docTypeSegment.selectedSegmentIndex = 0
           self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
           self.idFirstImageView.image = nil
           self.idSecondImageView.image = UIImage(named: "backCard")
           self.idSecondImageView.isHidden = true
           
           self.btnDoc.setTitleColor(.white, for: .normal)
            self.btnId.setTitleColor(.lightGray, for: .normal)
           self.isDocument = true
            self.idCardView.isHidden = true
           self.finalImageArray.removeAll()
           self.thumbCollectionView.reloadData()
           
           
       }
       
     
     
     @available(iOS 11.0, *)
     func makeThumbnail(pdfDocument: PDFDocument?, page: Int) -> UIImage? {
         return pdfDocument?.page(at: page)?.thumbnail(of: CGSize(width: self.attachmentImage.frame.width, height: self.attachmentImage.frame.height), for: .artBox)
     }
     
     
     @IBAction func cancelPressed(_ sender: Any) {
         
         self.isAttachmentEdited = false
         resetAllImageView()
         
         self.attachmentView.isHidden = true
     }
     
    
    
    @IBAction func sharePressed(_ sender: UIButton) {
        
        var imgURL:URL?
        
        if imageUrl != nil && attachUrl != ""{
            
          imgURL = URL(string: imageUrl+attachUrl!)!
            
        }
        
        

        
        if let refNumber = txtReferenceNumber.text {
           
            let refText = "Reference Number: " + "\(refNumber)"
            var attachment = ""
            let dueDate = "Due date: " + "\(txtDueDate.text ?? "")"
            let description = "Description: " + "\(txtDescription.text ?? "")"
            
            if let imageUrl = imgURL {
                
                  attachment = "Attachment : " + "\(imageUrl)"
                
            }
            
            
  
                let vc = UIActivityViewController(activityItems: [refText, dueDate, description], applicationActivities: [])
                present(vc, animated: true)
            }
            
            
            
        }
        
        
        
        
        
    
    
    
    
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        
        let newDate:String = ""
        
        self.txtDueDate.text = newDate.formatDate(date: datePicker.date)
        
        // self.dateStr = newDate.formatDate(date: datePicker.date)
    }
    
    @IBAction func fullViewRemoved(_ sender: Any) {
      //  self.imageFullView.isHidden = true
        
    }
    
    
    @IBAction func uploadPressed(_ sender: Any) {
        
        attachmentView.isHidden = false
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func removeImagePressed(_ sender: Any) {
        
       
      
        
        Alertift.alert(title: "Confirm", message: "Do you really want to remove attachment?")
            .action(.destructive("Delete")) { _, _, _ in
                
       
                
                if !(self.attachUrl?.isEmpty ?? false){
                    self.removeUploadedImage()
                    
                }else{
                    
                    self.removeAttachment() 
                    
                }
                
                
            }
            .action(.cancel("Cancel"))
            .show()
        
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
       
        if  Connectivity.isConnectedToInternet {
            
            
            
            if (txtReferenceNumber.text?.isEmpty)! ||  (txtDescription.text?.isEmpty)! ||  (txtDueDate.text?.isEmpty)! {
                
                
                
                 self.setToast(message:mandatoryFields,view:self.view,onController:self.navigationController!)
               // Alertift.alert(title: AppTitle, message:mandatoryFields)
                //    .action(.default("OK"))
                 //   .show(on: self)
                
                
            }else{
            
            
            guard let documentId = docId else{
                
                return
            }
            
            guard let companyId = companyId else{
                
                return
            }
            
            guard let docTypeId = docTypeId else {
                
                
                return
            }
                
               // print(txtDueDate.text!)
            
            SVProgressHUD.show()
            
            let params = [
                
                
                "document_id": documentId,
                "company_id":companyId,
                "document_type_id":docTypeId,
                "reference_no":self.txtReferenceNumber.text!,
                "description":self.txtDescription.text!,
                "expiry_date":self.txtDueDate.text!
                
                
                ] as [String : Any]
            
            
            self.deleteReminder.editDoc(withParameter: params) { (isSuccess, message) in
                
               
                if isSuccess {
                    
                    let param = ["document_id": documentId,
                                 "company_id":companyId,
                                 "document_type_id":docTypeId]
                    
                    if self.attachmentImage.image != nil && self.isAttachmentEdited! {
                        
                        self.updateImageFile(params: param)
                        
                    }else{
                        
                        SVProgressHUD.dismiss()
                        DispatchQueue.main.async {
                            
                            
                              self.setToast(message:message,view:self.view,onController:self.navigationController!)
                              self.navigationController?.popViewController(animated: true)
//                            Alertift.alert(title: AppTitle, message: message)
//                                .action(.default("OK"))
//                                .show(on: self)
                            
                        }
                        
                        
                        
                    }
                    
                    
                   
                    
                    
                }
                
            }
            }
        }else{
            
            
            
            self.setToast(message:noInternrt,view:self.view,onController:self.navigationController!)
            
//            Alertift.alert(title: AppTitle, message: noInternrt)
//                .action(.default("OK"))
//                .show(on: self)
            
            
        }
        
        
       
        
    }
    
    
    
    func updateImageFile(params:[String:String]) {
        
    
        
        if #available(iOS 11.0, *) {
            self.deleteReminder.updateImage(withParameter: params, imageArray: self.pdfDocument) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                
                if isSuccess{
                    
                    DispatchQueue.main.async {
                        
                        
                         self.setToast(message:message,view:self.view,onController:self.navigationController!)
                        
                         self.navigationController?.popViewController(animated: true)
//                        Alertift.alert(title: AppTitle, message: message)
//                            .action(.default("OK"))
//                            .show(on: self)
                        
                    }
                    
                   
                    
                    
                }
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        
    }
     
        
    
    func removeUploadedImage(){
        
        
        
        if  Connectivity.isConnectedToInternet {
            
            
            guard let documentId = docId else{
                
                return
            }
            
            guard let companyId = companyId else{
                
                return
            }
            
            guard let docTypeId = docTypeId else {
                
                
                return
            }
            
          //  SVProgressHUD.show()
            
            let params = [
                
                
                "document_id": documentId,
                "company_id":companyId,
                "document_type_id":docTypeId
                
                
                ] as [String : Any]
            
            
            self.deleteReminder.removeImage(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                if isSuccess {
                    
                    DispatchQueue.main.async {
                        
                        self.imagePickButton.isHidden = false
                        self.btnFullView.isHidden = true
                        self.btnRemove.isHidden = true
                         self.attachmentImage.image = nil
                        
                        self.setToast(message:message,view:self.view,onController:self.navigationController!)
                        
                    }
                    
                    if self.navigationController != nil{
                                                       
                        self.setToast(message:message,view:self.view,onController:self.navigationController!)
                                                       
                                                   }
                    
                    //self.navigationController?.popViewController(animated: true)
                    
                    
                }
                
            }
            
        }else{
            
            
            self.setToast(message:noInternrt, view: self.view,onController:self.navigationController!)
            
//            Alertift.alert(title: AppTitle, message: noInternrt)
//                .action(.default("OK"))
//                .show(on: self)
            
            
        }
        
        
        
        
    }
 
    
    
    
    func goForDelete(){
       
        
        
        if  Connectivity.isConnectedToInternet {
            
            
            guard let documentId = docId else{
                
                return
            }
            
            guard let companyId = companyId else{
                
                return
            }
            
            guard let docTypeId = docTypeId else {
                
                
                return
            }
            
           // SVProgressHUD.show()
            
            let params = [
                
                
                "document_id": documentId,
                "company_id":companyId,
                "document_type_id":docTypeId
                
                
                ] as [String : Any]
            
            
            self.deleteReminder.deleteDoc(withParameter: params) { (isSuccess, message) in
                
                SVProgressHUD.dismiss()
                if isSuccess {
                    
                    DispatchQueue.main.async {
                        
                        
                         self.setToast(message:message,view:self.view,onController:self.navigationController!)
                        
//                        Alertift.alert(title: AppTitle, message: message)
//                            .action(.default("OK"))
//                            .show(on: self)
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                    
                    
                    
                }
                
            }
            
        }else{
            
             self.setToast(message:noInternrt,view:self.view,onController:self.navigationController!)
            
//            Alertift.alert(title: AppTitle, message: noInternrt)
//                .action(.default("OK"))
//                .show(on: self)
            
            
        }
        
        
        
        
    }
    
    
     func removeAttachment() {
        
        //self.imageFullView.isHidden = true
       // self.btnRemove.isHidden = true
       
        
             // self.btnPDFView.isHidden = true
              
        if #available(iOS 11.0, *) {
            if self.pdfDocument.pageCount > 0{
                
                for i in 0..<self.pdfDocument.pageCount{
                    
                    pdfDocument.removePage(at: i)
                     self.imagePickButton.isHidden = false
                    self.btnRemove.isHidden = true
                    self.btnFullView.isHidden = true
                    
                }
                  self.attachmentImage.image = nil
                
            }
        } else {
            // Fallback on earlier versions
        }
              
        
    }
    
    
    @IBAction func deletepressed(_ sender: Any) {
        
        Alertift.alert(title: "Confirm", message: "Delete this Reminder?")
            .action(.destructive("Delete")) { _, _, _ in
            
                self.goForDelete()
            }
            .action(.cancel("Cancel"))
            .show()
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EditReminderViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
       

        
        var selectedImage: UIImage?
           if let editedImage = info[.editedImage] as? UIImage {
               selectedImage = editedImage
              
               picker.dismiss(animated: true, completion: nil)
           } else if let originalImage = info[.originalImage] as? UIImage {
               selectedImage = originalImage
              
               picker.dismiss(animated: true, completion: nil)
           }
           
        
        if GlobalDefault.bool(forKey: "isMonochrome") != nil{
                   
                   if GlobalDefault.bool(forKey: "isMonochrome"){
                       
                       
                       let bwImage = selectedImage?.blackAndWhite()
                       self.attachmentSelectedMainImage.image = bwImage
                       self.finalImageArray.append(bwImage!)
                       
                       
                   }else{
                       
                       self.attachmentSelectedMainImage.image = selectedImage
                       self.finalImageArray.append(selectedImage!)
                       
                       
                   }
                   
                   
               }else{
                   
                   self.attachmentSelectedMainImage.image = selectedImage
                   self.finalImageArray.append(selectedImage!)
                   
               }
               
              
        
        
        
        
        
        
        if self.finalImageArray.count > 0{
            
            if !isDocument{
                
                
                
                if self.finalImageArray.count > 1{
                    self.idFirstImageView.image = self.finalImageArray[0]
                    self.lblFrontSide.isHidden = true
                    self.idSecondImageView.image = self.finalImageArray[1]
                    self.lblBackSide.isHidden = true
                }else{
                    
                    if self.finalImageArray.count == 1{
                        self.idFirstImageView.image = self.finalImageArray[0]
                        self.lblFrontSide.isHidden = true
                        self.idSecondImageView.isHidden = false
                         self.lblBackSide.isHidden = false
                        
                    }
                    
             
                    
                }
                
            }
            
            
            self.thumbCollectionView.delegate = self
            self.thumbCollectionView.dataSource = self
            self.thumbCollectionView.reloadData()
            
        }
          
           
          
           //self.btnUpdate.setTitle("Attachment", for: .normal)
          // self.btnUpdate.isHidden = false
        
        
        
        
        
    }
    
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

@available(iOS 13.0, *)
extension EditReminderViewController:VNDocumentCameraViewControllerDelegate{
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        // Process the scanned pages
        
        var scannedImage:UIImage?
        
        for pageNumber in 0..<scan.pageCount {
            let image = scan.imageOfPage(at: pageNumber)
            scannedImage = image
        
             if let image = scannedImage{
                            
                            if GlobalDefault.bool(forKey: "isMonochrome") != nil{
                                
                                
                                if GlobalDefault.bool(forKey: "isMonochrome"){
                                    
                                    self.finalImageArray.append(image.blackAndWhite()!)
                                     scannedImage = image.blackAndWhite()!
                                    
                                }else{
                                    
                                    self.finalImageArray.append(image)
                                }
                                
                                
                            }else{
                               
                                   self.finalImageArray.append(image)
                                
                            }
                        
                          
                        }
                        
                
                        
                        if !isDocument{
                            
                            
                            self.idCardView.isHidden = false
                        
                            if self.finalImageArray.count > 0{
                                
                                
                              
                                        if self.finalImageArray.count == 1{
                                            
                                            self.idFirstImageView.image = self.finalImageArray[0]
                                            self.lblFrontSide.isHidden = true
                                            self.idSecondImageView.isHidden = false
                                            self.lblBackSide.isHidden = false
                                            
                                            
                                        }
                                        
                                        if self.finalImageArray.count > 1{
                                            
                                            self.idFirstImageView.image = self.finalImageArray[0]
                                            self.lblFrontSide.isHidden = true
                                            self.idSecondImageView.image = self.finalImageArray[1]
                                            self.lblBackSide.isHidden = true
                                            
                                            
                                        }
                                        

                            }
                            
                            
                        }
                        
                        else{
          
                        
                            
                            self.attachmentSelectedMainImage.image = scannedImage!
                            
                             self.idCardView.isHidden = true
                            
                        }
                        
        }
        
        
       //print(self.finalImageArray.count)
        

        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.thumbCollectionView.reloadData()
        // You are responsible for dismissing the controller.
        controller.dismiss(animated: true)
    }
    
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        // You are responsible for dismissing the controller.
        
        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.thumbCollectionView.reloadData()
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        // You should handle errors appropriately in your app.
    
        // You are responsible for dismissing the controller.
        controller.dismiss(animated: true)
    }
    
    
}

extension EditReminderViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !isDocument{
                   
                   if self.finalImageArray.count >= 2{
                       
                       return 2
                   }else{
                       
                        return self.finalImageArray.count + 1
                       
                   }
                   
                 
                   
               }else{
                   
                   return self.finalImageArray.count + 1
               }
               
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = thumbCollectionView.dequeueReusableCell(withReuseIdentifier: "thumbnailCell", for: indexPath) as! ThumbViewCell
        
        
       
        
        if indexPath.row < self.finalImageArray.count{
        
            
            cell.thumbPic.image = self.finalImageArray[indexPath.row]
            
            if cell.thumbPic.image != nil{
                
                cell.btnRemove.isHidden = false
                cell.indexValue = indexPath.row
                cell.delegate = self
               
                    
                    
            }else{
                
                
                 cell.btnRemove.isHidden = true
                
                
            }
            
            }else{
            
            cell.btnRemove.isHidden = true
            cell.thumbPic.image = UIImage(named: "ic_newScan")
            
        }
        
       
       
        
        return cell
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == self.finalImageArray.count{
            
            if self.isScan{
                
              goForscan()
                
            }else{
                
               
              getLibrary()
                
            }
            
            
            
        }else{
            
            self.attachmentSelectedMainImage.image = self.finalImageArray[indexPath.row]
            
        }
        
        
    }
    
    
    
    
}

extension EditReminderViewController:ThumbDeleteProtocol{
    
    func removeThumbnail(at index: Int) {
        
    
      
        self.finalImageArray.remove(at: index)
        self.thumbCollectionView.reloadData()
        
        if !isDocument{
        if finalImageArray.count == 1{
                       
                       self.idFirstImageView.image = finalImageArray[0]
                       self.idSecondImageView.isHidden = false
                       self.idSecondImageView.image = UIImage(named: "backCard")
                       self.lblBackSide.isHidden = false
                   }
                   
                   if finalImageArray.count >= 2 {
                       
                       self.idFirstImageView.image = finalImageArray[0]
                       self.idSecondImageView.image = finalImageArray[1]
                       self.idSecondImageView.isHidden = false
                       
                   }
            
            if finalImageArray.count == 0{
            
            self.idFirstImageView.image = UIImage(named: "backCard")
            self.idSecondImageView.image = UIImage(named: "backCard")
            self.idSecondImageView.isHidden = true
            self.lblBackSide.isHidden = true
            self.lblFrontSide.isHidden = false
            
            
            }
        
    
    }else{
            if finalImageArray.count >= 1{
                       
                     
                       if index > 0{
                          
                           self.attachmentSelectedMainImage.image = finalImageArray[index - 1]
                           
                       }else{
                           
                            self.attachmentSelectedMainImage.image = finalImageArray[index]
                            //self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
                       }
                      
                        
                       
                   }else{
                       
                       self.attachmentSelectedMainImage.image = UIImage(named: "scan_bg")
                       
                   }
                  
           
    }
    
    }
    
}

extension EditReminderViewController:  URLSessionDownloadDelegate {
    
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
       
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            
            if #available(iOS 11.0, *) {
                if let pdfDocument = PDFDocument(url:destinationURL) {
                    self.pdfDocument = pdfDocument
                    
                    DispatchQueue.main.async {
                        
                        
                        let image = self.makeThumbnail(pdfDocument: pdfDocument, page: 0)
                                                                self.attachmentImage.sd_setImage(with: URL(string: ""), placeholderImage: image)
                        
                                          self.btnRemove.isHidden = false
                                          self.pdfActivityIndicator.stopAnimating()
                                          self.pdfActivityIndicator.isHidden = true
                    }
                    
                  
                      
                }
            } else {
                // Fallback on earlier versions
            }
            
    
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
